package enterpriseManage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import structs.ChangeWorkInfo;
import structs.EmployeeInfo;
import structs.RecordInfo;
import structs.SalaryInfo;

public class DatabaseManage {

	private String[] DBemployeeinfo = { "EmpID", "EmpName", "DeptID",
			"HireTime", "Gender", "Birthday", "IDcard", "Age",
			"PoliticalParty", "Education", "Telephone" };

	private String[] DBsalaryinfo = { "empID", "DeptID", "Year", "Month",
			"BasicSalary", "Bonus", "Fine", "SumSalary" };

	private String[] DBRecordinfo = { "EmpID", "DeptID", "Day", "SignInTime",
			"SignOutTime", "state" };

	private String[] DBChangeinfo = { "EmpID", "PreDeptID", "NewDeptID",
			"PreDeptID", "NewDutyID", "ChangeTime" };

	Connection conn = null;

	public DatabaseManage() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("注册数据库成功");

			this.conn = DriverManager
					.getConnection("jdbc:mysql://swj.club/pms?user=htliu&password=hehe");
			System.out.println("链接数据库成功");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void disconnect() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 登录检测方法
	 * 
	 * @param userID
	 *            用户名(员工号)
	 * @param password
	 *            密码
	 * @param permission
	 *            身份权限
	 * @return 是否登录成功
	 */
	public boolean isLogin(String userID, String password, int permission) {
		boolean found = false;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select userID from user where userID=" + userID
				+ " and password=" + password + " and permission=" + permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			if (!rs.next()) {
				found = false;
			} else {
				found = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return found;
	}

	/**
	 * 查询员工自己的人事信息
	 * 
	 * @param userID
	 *            用户名(员工号)
	 * @return 该员工的人事信息
	 */
	public EmployeeInfo searchEmpInfoByUserID(String userID) {
		EmployeeInfo emp = new EmployeeInfo();
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from employeeinfo,dept where employeeinfo.DeptID=dept.DeptID and empID="
				+ userID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				emp.setEmpID(rs.getString(DBemployeeinfo[0]));
				emp.setEmpName(rs.getString(DBemployeeinfo[1]));
				emp.setDept(rs.getString("deptName"));
				emp.setHireTime(rs.getString(DBemployeeinfo[3]));
				emp.setGender(rs.getString(DBemployeeinfo[4]));
				emp.setBirthday(rs.getString(DBemployeeinfo[5]));
				emp.setIDcard(rs.getString(DBemployeeinfo[6]));
				emp.setAge(rs.getString(DBemployeeinfo[7]));
				emp.setPoliticalParty(rs.getString(DBemployeeinfo[8]));
				emp.setEducation(rs.getString(DBemployeeinfo[9]));
				emp.setTelephone(rs.getString(DBemployeeinfo[10]));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return emp;
	}

	/**
	 * 查询某员工的人事信息
	 * 
	 * @param empID
	 *            用户名(员工号)
	 * @return 该员工的人事信息
	 */
	public EmployeeInfo[] searchEmpInfoByEmpID(String empID) {
		EmployeeInfo[] emps = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from employeeinfo,dept where employeeinfo.DeptID=dept.DeptID and empID="
				+ empID;
		System.out.println(sql);
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);

			rs = stat.executeQuery(sql);
			rs.last();
			System.out.println(rs.getRow());
			int length = rs.getRow();
			emps = new EmployeeInfo[length];
			rs.beforeFirst();
			for (int i = 0; i < length; i++) {
				emps[i] = new EmployeeInfo();
			}

			int i = 0;
			while (rs.next()) {
				emps[i].setEmpID(rs.getString(DBemployeeinfo[0]));
				emps[i].setEmpName(rs.getString(DBemployeeinfo[1]));
				emps[i].setDept(rs.getString("deptName"));
				emps[i].setHireTime(rs.getString(DBemployeeinfo[3]));
				emps[i].setGender(rs.getString(DBemployeeinfo[4]));
				emps[i].setBirthday(rs.getString(DBemployeeinfo[5]));
				emps[i].setIDcard(rs.getString(DBemployeeinfo[6]));
				emps[i].setAge(rs.getString(DBemployeeinfo[7]));
				emps[i].setPoliticalParty(rs.getString(DBemployeeinfo[8]));
				emps[i].setEducation(rs.getString(DBemployeeinfo[9]));
				emps[i].setTelephone(rs.getString(DBemployeeinfo[10]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return emps;
	}

	/**
	 * 查询部门员工所有的人事信息
	 * 
	 * @param deptID
	 *            部门号
	 * @return 该部门所有员工
	 */
	public EmployeeInfo[] searchAllEmpInfoByDeptID(String deptID) {
		EmployeeInfo[] emps = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from employeeinfo,dept where employeeinfo.DeptID=dept.DeptID and employeeinfo.DeptID="
				+ deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			emps = new EmployeeInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				emps[i] = new EmployeeInfo();
			}

			int i = 0;
			while (rs.next()) {
				emps[i].setEmpID(rs.getString(DBemployeeinfo[0]));
				emps[i].setEmpName(rs.getString(DBemployeeinfo[1]));
				emps[i].setDept(rs.getString("DeptName"));
				emps[i].setHireTime(rs.getString(DBemployeeinfo[3]));
				emps[i].setGender(rs.getString(DBemployeeinfo[4]));
				emps[i].setBirthday(rs.getString(DBemployeeinfo[5]));
				emps[i].setIDcard(rs.getString(DBemployeeinfo[6]));
				emps[i].setAge(rs.getString(DBemployeeinfo[7]));
				emps[i].setPoliticalParty(rs.getString(DBemployeeinfo[8]));
				emps[i].setEducation(rs.getString(DBemployeeinfo[9]));
				emps[i].setTelephone(rs.getString(DBemployeeinfo[10]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return emps;
	}

	/**
	 * 查询部门号
	 * 
	 * @param empID
	 *            员工
	 * @return 部门号
	 */
	public String searchDeptIDByempID(String empID) {
		String deptID = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select deptID from employeeinfo where empID=" + empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				deptID = rs.getString("DeptID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return deptID;
	}

	/**
	 * 查询部门号
	 * 
	 * @param deptName
	 *            部门名
	 * @return 部门号
	 */
	public String searchDeptIdByDeptName(String deptName) {
		String deptID = null;
		Statement stat = null;
		ResultSet rs = null;
	//	deptName = "ai";
				
		String sql = "select DeptID from dept where DeptName=" + "'"
				+ deptName + "'";

	   // System.out.println(sql);
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
		//	System.out.println(rs.getRow());
			rs.beforeFirst();
			while (rs.next()) {
				deptID = rs.getString("DeptID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return deptID;
	}

	/**
	 * 查询部门所有员工号
	 * 
	 * @param deptID
	 *            部门号
	 * @return 部门名
	 */
	public String[] searchAllEmpIDBydeptID(String deptID, String permission) {
		String[] empIDs = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select EmpID from employeeinfo where DeptID=" + deptID
				+ " and Permission=" + permission;
		// System.out.println(sql);
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);

			rs.last();
			int length = rs.getRow();
			empIDs = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				empIDs[i] = rs.getString("EmpID");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return empIDs;
	}

	/**
	 * 查询部门名字
	 * 
	 * @param deptID
	 *            部门号
	 * @return 部门名
	 */
	public String searchDeptNameBydeptID(String deptID) {
		String deptName = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select deptName from dept where deptID=" + deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				deptName = rs.getString("DeptName");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return deptName;
	}

	/**
	 * 查询员工姓名
	 * 
	 * @param empID
	 *            员工号
	 * @return 员工姓名
	 */
	public String searchEmpNameByEmpID(String empID) {
		String empName = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select EmpName from employeeinfo where EmpID=" + empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				empName = rs.getString("EmpName");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return empName;
	}

	/**
	 * 查询所有部门名称
	 * 
	 * @return 所有部门名
	 */
	public String[] searchAllDeptName() {

		String[] deptNames = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from dept ";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);

			rs.last();
			int length = rs.getRow();
			deptNames = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				deptNames[i] = rs.getString("DeptName");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return deptNames;
	}

	/**
	 * 员工签到
	 * 
	 * @param empID
	 *            员工号
	 * @param deptID
	 *            部门号
	 * @param day
	 *            签到天数
	 * @param signInTime
	 *            签到时间
	 * @return 是否签到成功
	 */
	public boolean signIn(String empID, String deptID, String day,
			String signInTime) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into workrecord (empid,deptid,day,signintime,signouttime,state) values(?,?,?,?,?,?)";
		System.out.println(empID+" "+deptID+" "+day+" "+signInTime);
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, empID);
			pstat.setString(2, deptID);
			pstat.setString(3, day);
			pstat.setString(4, signInTime);
			pstat.setString(5, null);
			pstat.setString(6, null);
			System.out.println(pstat);
			int i = pstat.executeUpdate();
			if (i == 1) {
				success = true;
			}
			if(success)
			System.out.println("fuck");
		} catch (SQLException e) {
			System.out.println(e);
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 员工签离
	 * 
	 * @param empID
	 *            员工
	 * @param deptID
	 *            部门号
	 * @param day
	 *            签出日期
	 * @param signOutTime
	 *            签离时间
	 * @return 签离是否成功
	 */
	public boolean signOut(String empID, String deptID, String day,
			String signOutTime) {
		boolean success = false;
		Statement stat = null;
		String sql = "update workrecord set SignOutTime=" + "\'" + signOutTime
				+ "\'" + " where EmpID=" + empID + " and DeptID=" + deptID
				+ " and Day=" + "\'" + day + "\'";
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i >= 0) {
				success = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	/**
	 * 添加用户
	 * 
	 * @param emp
	 *            员工
	 * @return 是否成功
	 */
	public boolean addNewUsrToDb(EmployeeInfo emp) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into user values(?,?,?)";
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, emp.getEmpID());
			pstat.setString(2, emp.getPassword());
			pstat.setString(3, String.valueOf(emp.getPermission()));
			System.out.println(pstat);
			int i = pstat.executeUpdate();
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 添加员工人事信息
	 * 
	 * @param emp
	 *            员工
	 * @return 是否成功
	 */
	public boolean addNewEmpInfoToDB(EmployeeInfo emp) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into employeeinfo values(?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, emp.getEmpID());
			pstat.setString(2, emp.getEmpName());
			pstat.setString(3, emp.getGender());
			pstat.setString(4, emp.getBirthday());
			pstat.setString(5, emp.getAge());
			pstat.setString(6, emp.getPoliticalParty());
			pstat.setString(7, emp.getDept());
			pstat.setString(8, emp.getIDcard());
			pstat.setString(9, String.valueOf(emp.getPermission()));
			pstat.setString(10, emp.getHireTime());
			pstat.setString(11, emp.getTelephone());
			//pstat.setString(12, );
			pstat.setString(12, emp.getEducation());

		//	System.out.println(emp.getEmpID()+" "+emp.getEmpName()+" "+emp.getGender()+" "+emp.getBirthday()+" "+emp.getAge()+" "+emp.getPoliticalParty()+" "+emp.getDept()+" "+emp.getIDcard()+" "+emp.getPermission()+" "+emp.getHireTime()+" "+emp.getTelephone()+" "+emp.getEducation());
			System.out.println(pstat);
			int i = pstat.executeUpdate();
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 查询部门员工所有的工资
	 * 
	 * @param deptID
	 *            部门号
	 * @return 该部门所有员工的工资信息
	 */
	public SalaryInfo[] searchAllSalaryInfoByDeptID(String deptID,
			String permission) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select distinct * from salary,employeeinfo,dept where salary.DeptID=dept.DeptID and salary.DeptID=employeeinfo.DeptID and salary.DeptID="
				+ deptID + " and Permission=" + permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString("deptName"));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询某员工的工资记录
	 * 
	 * @param empID
	 *            员工号
	 * @return 该员工的工资记录
	 */
	public SalaryInfo[] searchEmpSalaryByEmpID(String empID) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from salary,dept where salary.DeptID=dept.DeptID and EmpID="
				+ empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString("DeptName"));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询某员工的工资记录
	 * 
	 * @param empID
	 *            员工号
	 * @param permission
	 *            查询权限
	 * @return 该员工的工资记录
	 */
	public SalaryInfo[] searchEmpSalaryByDate(String deptID, String year,
			String month, String permission) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from salary,dept,employeeinfo where salary.EmpID=employeeinfo.EmpID and salary.DeptID=dept.DeptID and Year="
				+ "\'"
				+ year
				+ "\'"
				+ " and Month="
				+ "\'"
				+ month
				+ "\'"
				+ " and Permission="
				+ permission
				+ " and dept.DeptID="
				+ deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString("DeptName"));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询某员工的考勤记录
	 * 
	 * @param empID
	 *            员工号
	 * @return 该员工的考勤记录
	 */
	public RecordInfo[] searchEmpWorkRecordByEmpID(String empID) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord,dept where dept.DeptID=workrecord.DeptID and empID="
				+ empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString("DeptName"));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询考勤表里的所有日期
	 * 
	 * @return 考勤表里的所有日期
	 */
	public String[] searchWorkRecordDate() {
		String[] workDates = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select distinct Day from workrecord";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			workDates = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				workDates[i] = rs.getString("Day");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return workDates;
	}

	/**
	 * 查询工资表里的所有年份
	 * 
	 * @return 工资表里的所有年份
	 */
	public String[] searchSalaryYear() {
		String[] salaryYears = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select distinct Year from salary";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salaryYears = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				salaryYears[i] = rs.getString("Year");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salaryYears;
	}

	/**
	 * 查询工资表里的所有月份
	 * 
	 * @return 工资表里的所有月份
	 */
	public String[] searchSalaryMonth() {
		String[] salaryMonths = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select distinct  Month from salary";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salaryMonths = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				salaryMonths[i] = rs.getString("Month");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salaryMonths;
	}

	/**
	 * 查询某员工的考勤记录
	 * 
	 * @param date
	 *            日期
	 * @param Permission
	 *            查询权限
	 * @return 该员工的考勤记录
	 */
	public RecordInfo[] searchEmpWorkRecordByDate(String deptID, String date,
			String Permission) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord,employeeinfo,dept where dept.DeptID=employeeinfo.DeptID and workrecord.empID=employeeinfo.EmpID and Permission="
				+ Permission
				+ " and Day="
				+ "\'"
				+ date
				+ "\'"
				+ " and workrecord.DeptID=" + deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString("DeptName"));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询某部门所有员工的考勤记录
	 * 
	 * @param deptID
	 *            部门号
	 * @param permission
	 *            部门号
	 * @return 所有员工的考勤记录
	 */
	public RecordInfo[] searchAllEmpWorkRecordByDeptID(String deptID,
			String permission) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord,employeeinfo,dept where dept.DeptID = workrecord.DeptID and workrecord.empID=employeeinfo.EmpID and workrecord.DeptID="
				+ deptID + " and Permission=" + permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString("DeptName"));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询某员工的调岗记录
	 * 
	 * @param empID
	 *            员工号
	 * @return 该员工的调岗记录记录
	 */
	public ChangeWorkInfo[] searchEmpChangeWorkByEmpID(String empID) {
		ChangeWorkInfo[] changeInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from changework where EmpID=" + empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			changeInfos = new ChangeWorkInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				changeInfos[i] = new ChangeWorkInfo();
			}

			int i = 0;
			while (rs.next()) {
				changeInfos[i].setEmpID(rs.getString(DBChangeinfo[0]));
				changeInfos[i].setPreDeptID(rs.getString(DBChangeinfo[1]));
				changeInfos[i].setNewDeptID(rs.getString(DBChangeinfo[2]));
				changeInfos[i].setPreDutyID(rs.getString(DBChangeinfo[3]));
				changeInfos[i].setNewDutyID(rs.getString(DBChangeinfo[4]));
				changeInfos[i].setChangeTime(rs.getString(DBChangeinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return changeInfos;
	}

	/**
	 * 查询所有员工的调岗记录
	 * 
	 * @return 所有员工的调岗记录
	 */
	public ChangeWorkInfo[] searchAllEmpChangeWork() {
		ChangeWorkInfo[] changeInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from changework";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			changeInfos = new ChangeWorkInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				changeInfos[i] = new ChangeWorkInfo();
			}

			int i = 0;
			while (rs.next()) {
				changeInfos[i].setEmpID(rs.getString(DBChangeinfo[0]));
				changeInfos[i].setPreDeptID(rs.getString(DBChangeinfo[1]));
				changeInfos[i].setNewDeptID(rs.getString(DBChangeinfo[2]));
				changeInfos[i].setPreDutyID(rs.getString(DBChangeinfo[3]));
				changeInfos[i].setNewDutyID(rs.getString(DBChangeinfo[4]));
				changeInfos[i].setChangeTime(rs.getString(DBChangeinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return changeInfos;
	}

	/**
	 * 查询员工的职位号
	 * 
	 * @param empID
	 *            员工号
	 * @param deptID
	 *            部门号
	 * @return 职位号
	 */
	public String searchDutyIDByempIDdeptID(String empID, String deptID) {
		String dutyID = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select dutyID from work where empID=" + empID
				+ " and deptID=" + deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				dutyID = rs.getString("dutyID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dutyID;
	}

	/**
	 * 查询职位名
	 * 
	 * @param dutyID
	 *            职位号
	 * @return 职位名
	 */
	public String searchDutyNameBydutyID(String dutyID) {
		String dutyName = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select dutyName from duty where DutyID=" + dutyID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				dutyName = rs.getString("dutyName");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dutyName;
	}

	/**
	 * 删除用户信息
	 * 
	 * @param userID
	 *            用户工号
	 * @return 是否成功
	 */
	public boolean deleteUserByUserID(String userID) {
		boolean success = false;
		Statement stat = null;
		String sql = "delete from user where userID=" + userID;
		System.out.println(sql);
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 删除员工人事信息
	 * 
	 * @param empID
	 *            员工工号
	 * @return 是否成功
	 */
	public boolean deleteEmpInfoByEmpID(String empID) {
		boolean success = false;
		Statement stat = null;
		String sql = "delete from employeeinfo where EmpID=" + empID;
		System.out.println(sql);
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			System.out.println(stat);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 删除员工工资信息
	 * 
	 * @param empID
	 *            员工工号
	 * @return 是否成功
	 */
	public boolean deleteSalaryByEmpID(String empID) {
		boolean success = false;
		Statement stat = null;
		String sql = "delete from salary where EmpID=" + empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 删除员工考勤信息
	 * 
	 * @param empID
	 *            员工工号
	 * @return 是否成功
	 */
	public boolean deleteWorkRecordByEmpID(String empID) {
		boolean success = false;
		Statement stat = null;
		String sql = "delete from workrecord where EmpID=" + empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 删除员工工作信息
	 * 
	 * @param empID
	 *            员工工号
	 * @return 是否成功
	 */
	public boolean deleteWorkByEmpID(String empID) {
		boolean success = false;
		Statement stat = null;
		String sql = "delete from work where userID=" + empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 查询员工自身的工资记录
	 * 
	 * @param empID
	 *            员工号
	 * @param year
	 *            年
	 * @param month
	 *            月
	 * @return 该员工的工资记录
	 */
	public SalaryInfo[] searchSelfSalaryByDate(String empID, String year,
			String month) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from salary where EmpID=" + empID + " and Year="
				+ "\'" + year + "\'" + " and Month=" + "\'" + month + "\'";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString(DBsalaryinfo[1]));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询员工自身的考勤记录
	 * 
	 * @param empID
	 *            员工号
	 * @param date
	 *            日期
	 * @return 该员工的考勤记录
	 */
	public RecordInfo[] searchSelfWorkRecordByDate(String empID, String date) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord where EmpID=" + empID
				+ " and Day=" + "\'" + date + "\'";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString(DBRecordinfo[1]));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询所有部门经理员工号
	 * 
	 * @param permission
	 *            查询权限
	 * @return 所有部门经理的员工号
	 */
	public String[] searchAllDiEmpIDByPermission(String permission) {
		String[] empIDs = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select EmpID from employeeinfo where Permission="
				+ permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);

			rs.last();
			int length = rs.getRow();
			empIDs = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				empIDs[i] = rs.getString("EmpID");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return empIDs;
	}

	/**
	 * 查询所有部门经理的人事信息
	 * 
	 * @param permission
	 *            查询权限
	 * @return 该部门所有员工
	 */
	public EmployeeInfo[] searchAllDiEmpInfoByPermission(String permission) {
		EmployeeInfo[] emps = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from employeeinfo,dept where employeeinfo.DeptID=dept.DeptID and Permission="
				+ permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			emps = new EmployeeInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				emps[i] = new EmployeeInfo();
			}

			int i = 0;
			while (rs.next()) {
				emps[i].setEmpID(rs.getString(DBemployeeinfo[0]));
				emps[i].setEmpName(rs.getString(DBemployeeinfo[1]));
				emps[i].setDept(rs.getString("DeptName"));
				emps[i].setHireTime(rs.getString(DBemployeeinfo[3]));
				emps[i].setGender(rs.getString(DBemployeeinfo[4]));
				emps[i].setBirthday(rs.getString(DBemployeeinfo[5]));
				emps[i].setIDcard(rs.getString(DBemployeeinfo[6]));
				emps[i].setAge(rs.getString(DBemployeeinfo[7]));
				emps[i].setPoliticalParty(rs.getString(DBemployeeinfo[8]));
				emps[i].setEducation(rs.getString(DBemployeeinfo[9]));
				emps[i].setTelephone(rs.getString(DBemployeeinfo[10]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return emps;
	}

	/**
	 * 查询所有部门经理的工资记录
	 * 
	 * @param permission
	 *            查询权限
	 * @return 该员工的工资记录
	 */
	public SalaryInfo[] searchDiEmpSalaryByDate(String year, String month,
			String permission) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from salary,dept,employeeinfo where salary.EmpID=employeeinfo.EmpID and salary.DeptID=dept.DeptID and Year="
				+ "\'"
				+ year
				+ "\'"
				+ " and Month="
				+ "\'"
				+ month
				+ "\'"
				+ " and Permission=" + permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString("DeptName"));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询所有部门经理的工资
	 * 
	 * @param permission
	 *            查询权限
	 * @return 该部门所有员工的工资信息
	 */
	public SalaryInfo[] searchAllDiSalaryInfoByPermission(String permission) {
		SalaryInfo[] salarys = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from salary,employeeinfo,dept where salary.DeptID=dept.DeptID and salary.EmpID=employeeinfo.EmpID and Permission="
				+ permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			salarys = new SalaryInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				salarys[i] = new SalaryInfo();
			}

			int i = 0;
			while (rs.next()) {
				salarys[i].setEmpID(rs.getString(DBsalaryinfo[0]));
				salarys[i].setDeptID(rs.getString("deptName"));
				salarys[i].setYear(rs.getString(DBsalaryinfo[2]));
				salarys[i].setMonth(rs.getString(DBsalaryinfo[3]));
				salarys[i].setBasicSalary(rs.getString(DBsalaryinfo[4]));
				salarys[i].setBonus(rs.getString(DBsalaryinfo[5]));
				salarys[i].setFine(rs.getString(DBsalaryinfo[6]));
				salarys[i].setSumSalary(rs.getString(DBsalaryinfo[7]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return salarys;
	}

	/**
	 * 查询某员工的考勤记录
	 * 
	 * @param date
	 *            日期
	 * @param Permission
	 *            查询权限
	 * @return 该员工的考勤记录
	 */
	public RecordInfo[] searchDiEmpWorkRecordByDate(String date,
			String Permission) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord,employeeinfo,dept where dept.DeptID=employeeinfo.DeptID and workrecord.empID=employeeinfo.EmpID and Permission="
				+ Permission + " and Day=" + "\'" + date + "\'";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString("DeptName"));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询所有部门经理的考勤记录
	 * 
	 * @param permission
	 *            部门号
	 * @return 所有员工的考勤记录
	 */
	public RecordInfo[] searchAllDiEmpWorkRecordByPermission(String permission) {
		RecordInfo[] recordInfos = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from workrecord,employeeinfo,dept where dept.DeptID = workrecord.DeptID and workrecord.empID=employeeinfo.EmpID and Permission="
				+ permission;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			rs.last();
			int length = rs.getRow();
			recordInfos = new RecordInfo[length];
			rs.beforeFirst();

			for (int i = 0; i < length; i++) {
				recordInfos[i] = new RecordInfo();
			}

			int i = 0;
			while (rs.next()) {
				recordInfos[i].setEmpID(rs.getString(DBRecordinfo[0]));
				recordInfos[i].setDeptID(rs.getString("DeptName"));
				recordInfos[i].setDay(rs.getString(DBRecordinfo[2]));
				recordInfos[i].setSignIntime(rs.getString(DBRecordinfo[3]));
				recordInfos[i].setSignOutTime(rs.getString(DBRecordinfo[4]));
				recordInfos[i].setState(rs.getString(DBRecordinfo[5]));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recordInfos;
	}

	/**
	 * 查询员工职位名
	 * 
	 * @param empID
	 *            员工号
	 * @param deptID
	 *            部门号
	 * @return 职位名
	 */
	public String searchDutyNameByEmpIDDeptID(String empID, String deptID) {
		String dutyName = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select DutyName from work,duty where work.DutyID=duty.DutyID and work.userID="
				+ empID + " and work.DeptID=" + deptID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				dutyName = rs.getString("DutyName");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dutyName;
	}

	/**
	 * 查询员工密码
	 * 
	 * @param empID
	 *            员工号
	 * @return 密码
	 */
	public String searchEmpPasswordByEmpID(String empID) {
		String password = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select password from user where userID=" + empID;
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				password = rs.getString("password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return password;
	}

	/**
	 * 修改密码
	 * 
	 * @param empID
	 *            员工
	 * @param newPassword
	 *            新密码
	 * @return 签离是否成功
	 */
	public boolean updatePassword(String empID, String newPassword) {
		boolean success = false;
		Statement stat = null;
		String sql = "update user set password=" + newPassword
				+ " where userID=" + empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	/**
	 * 查询职位号
	 * 
	 * @param dutyName
	 *            职位名
	 * @return 职位号
	 */
	public String searchDutyIdByDutyName(String dutyName) {
		String dutyID = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select DutyID from duty where DutyName=" + "\'"
				+ dutyName + "\'";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			while (rs.next()) {
				dutyID = rs.getString("DutyID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dutyID;
	}

	/**
	 * 添加员工工作信息
	 * 
	 * @param emp
	 *            员工
	 * @return 是否成功
	 */
	public boolean addNewWorkInfoToDB(EmployeeInfo emp) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into work values(?,?,?)";
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, emp.getEmpID());
			pstat.setString(2, emp.getDept());
			pstat.setString(3, "1");//emp.getDutyID());
			
			System.out.println(pstat);
			int i = pstat.executeUpdate();
			System.out.println(i);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 添加员工工资信息
	 * 
	 * @param salary
	 *            工资
	 * @return 是否成功
	 */
	public boolean addNewSalaryInfoToDB(SalaryInfo salary) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into salary values(?,?,?,?,?,?,?,?)";
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, salary.getEmpID());
			pstat.setString(2, salary.getDeptID());
			pstat.setString(7, salary.getYear());
			pstat.setString(8, salary.getMonth());
			pstat.setString(3, salary.getBasicSalary());
			pstat.setString(4, salary.getBonus());
			pstat.setString(5, salary.getFine());
			pstat.setString(6, salary.getSumSalary());
      //      System.out.println(salary.getEmpID()+" "+salary.getDeptID()+" "+salary.getYear()+" "+salary.getMonth()+" "+salary.getBasicSalary()+" "+salary.getBonus()+" "+salary.getFine()+" "+salary.getSumSalary());
			int i = pstat.executeUpdate();
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 查询所有职位名称
	 * 
	 * @return 所有职位名
	 */
	public String[] searchAllDutyName() {

		String[] dutyNames = null;
		Statement stat = null;
		ResultSet rs = null;
		String sql = "select * from duty ";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);

			rs.last();
			int length = rs.getRow();
			dutyNames = new String[length];
			rs.beforeFirst();

			int i = 0;
			while (rs.next()) {
				dutyNames[i] = rs.getString("DutyName");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stat != null) {
					stat.close();
					stat = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dutyNames;
	}

	/**
	 * 添加员工调岗信息
	 * 
	 * @param change
	 *            调岗信息
	 * @return 是否成功
	 */
	public boolean addChangeWorkInfoToDB(ChangeWorkInfo change) {
		boolean success = false;
		PreparedStatement pstat = null;
		String sql = "insert into changework values(?,?,?,?,?,?)";
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, change.getEmpID());
			pstat.setString(2, change.getPreDeptID());
			pstat.setString(3, change.getNewDeptID());
			pstat.setString(4, change.getPreDutyID());
			pstat.setString(5, change.getNewDutyID());
			pstat.setString(6, change.getChangeTime());

			int i = pstat.executeUpdate();
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (pstat != null)
					pstat.close();
			} catch (SQLException e) {
				return false;
			}
		}
		return success;
	}

	/**
	 * 更新employeeinfo表信息
	 * 
	 * @param empID
	 *            员工
	 * @param deptID
	 *            部门号
	 * @return 是否成功
	 */
	public boolean updateEmployeeinfo(String empID, String deptID) {
		boolean success = false;
		Statement stat = null;
		String sql = "update employeeinfo set DeptID=" + "\'"+deptID+"\'"
				+ " where EmpID=" + empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			if (i == 1) {
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	/**
	 * 更新work表信息
	 * 
	 * @param empID
	 *            员工
	 * @param deptID
	 *            部门号
	 * @param dutyID
	 *            职位号
	 * @return 是否成功
	 */
	public boolean updateWork(String empID, String deptID, String dutyID) {
		boolean success = false;
		Statement stat = null;
		String sql = "update work set DeptID=" +"\'"+ deptID +"\'"+ " where userID=" + empID;
		String sql2= "update work set DutyID="+"\'"+dutyID+"\'"+"where userID="+empID;
		try {
			stat = conn.createStatement();
			int i = stat.executeUpdate(sql);
			int j=stat.executeUpdate(sql2);
			System.out.println(i+"   "+j);
			
			if (i == 1&&j==1) {
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

}
