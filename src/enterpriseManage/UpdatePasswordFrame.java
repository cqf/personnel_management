package enterpriseManage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class UpdatePasswordFrame extends JFrame{
	
	private static final int WIDTH=400;
	private static final int HEIGHT = 350;
	
	private JLabel title_label;
	
	private JLabel prePassword_label;
	private JLabel newPassword_label;
	private JLabel confirm_label;
	
	private JPasswordField prePassword_field;
	private JPasswordField newPassword_field;
	private JPasswordField confirm_field;
	
	private JButton confirm_button;
	
	String empID;
	DatabaseManage dbm;
	
	public UpdatePasswordFrame(String empID,DatabaseManage dbm){
		this.setBounds(300, 50, WIDTH, HEIGHT);
		this.setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);
		
		this.empID=empID;
		this.dbm=dbm;
		
		title_label=new JLabel("修改密码");
		title_label.setFont(new Font("楷体", Font.BOLD, 22));
		title_label.setBounds(150, 20, 300, 50);
		
		prePassword_label=new JLabel("原密码");
		prePassword_label.setBounds(50,80,50,30);
		
		newPassword_label=new JLabel("新密码");
		newPassword_label.setBounds(50,130,50,30);
		
		confirm_label=new JLabel("再输入一次新密码");
		confirm_label.setBounds(50,180,120,30);
		
		prePassword_field=new JPasswordField();
		prePassword_field.setBounds(200, 80, 100, 30);
		
		newPassword_field=new JPasswordField();
		newPassword_field.setBounds(200, 130, 100, 30);
		
		confirm_field=new JPasswordField();
		confirm_field.setBounds(200, 180, 100, 30);
		
		confirm_button=new JButton("确定");
		confirm_button.setBounds(150,250,100,30);
		confirm_button.addActionListener(new MyButtonListener());
		
		this.setBackground(Color.white);
		content.add(title_label);
		content.add(prePassword_label);
		content.add(newPassword_label);
		content.add(confirm_label);
		content.add(prePassword_field);
		content.add(newPassword_field);
		content.add(confirm_field);
		content.add(confirm_button);	
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==confirm_button){
				String preP=prePassword_field.getText();
				String newP=newPassword_field.getText();
				String conP=newPassword_field.getText();
				String havP=dbm.searchEmpPasswordByEmpID(empID);
				
				if(!(newP.equals(conP))){
					JOptionPane.showMessageDialog(null, "两次新密码输入不一致", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else if(!(preP.equals(havP))){
					JOptionPane.showMessageDialog(null, "原密码输入错误", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else{
					boolean b=dbm.updatePassword(empID, newP);
					if(b){
						JOptionPane.showMessageDialog(null, "修改成功", "Information",
								JOptionPane.INFORMATION_MESSAGE);
						UpdatePasswordFrame.this.setVisible(false);
					}else{
						JOptionPane.showMessageDialog(null, "修改失败", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}			
			}
		}
		
	}

}
