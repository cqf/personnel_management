package enterpriseManage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ManagerFrame extends JFrame {

	String userID;
	int permission;
	DatabaseManage dbm;
	String belongPermission;

	private String[] operations = { "人事管理", "工资管理", "考勤管理", "查询调岗" };

	private static final int WIDTH = 900; // 界面宽度
	private static final int HEIGHT = 600; // 界面高度
	private static final int OPERATEPANEL_WIDTH = 800;
	private static final int OPERATEPANEL_HEIGHT = 300;

	private JLabel titleLabel = null;

	private EmpInfoPanel empInfoPanel = null;
	private JTabbedPane operatePaneTabs = null;

	private EmpManagePanel empManagePanel = null;
	private SalaryManagePanel salaryPanel = null;
	private WorkRecordManagePanel recordPanel = null;
	private ChangeWorkPanel changePanel = null;

	public ManagerFrame(String userID, int permission, DatabaseManage dbm) {
		this.userID = userID;
		this.permission = permission;
		this.dbm = dbm;

		if (permission == Constant.UserPermission.STAFF_RIGHT) {
			belongPermission =String.valueOf(Constant.UserPermission.STAFF_RIGHT);
		} else {
			belongPermission =String.valueOf(permission + 1);
		}

		this.setTitle("人力资源管理系统");
		this.setBounds(300, 50, WIDTH, HEIGHT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);
		
		ImageIcon img = new ImageIcon("src/Images//22.jpg");
		JLabel background = new JLabel(img);
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());

		titleLabel = new JLabel("人力资源管理系统");
		titleLabel.setFont(new Font("楷体", Font.BOLD, 22));
		titleLabel.setBounds(300, 20, 300, 50);

		empInfoPanel = new EmpInfoPanel(userID, permission, dbm);

		operatePaneTabs = new JTabbedPane();
		operatePaneTabs.setBounds(50, 220, OPERATEPANEL_WIDTH,
				OPERATEPANEL_HEIGHT);

		empManagePanel = new EmpManagePanel(userID, permission, dbm,belongPermission);
		operatePaneTabs.addTab(operations[0], empManagePanel);

		salaryPanel = new SalaryManagePanel(userID, permission, dbm,belongPermission);
		operatePaneTabs.addTab(operations[1], salaryPanel);

		recordPanel = new WorkRecordManagePanel(userID, permission, dbm,belongPermission);
		operatePaneTabs.addTab(operations[2], recordPanel);

		changePanel = new ChangeWorkPanel(userID, permission, dbm,belongPermission);
		operatePaneTabs.addTab(operations[3], changePanel);

		operatePaneTabs.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if (((JTabbedPane) e.getSource()).getSelectedIndex() == 2) {
					recordPanel.update();
				} else if (((JTabbedPane) e.getSource()).getSelectedIndex() == 3) {
					changePanel.update();
				}
			}

		});

		operatePaneTabs.setBackground(Color.white);
		content.add(titleLabel);
		content.add(empInfoPanel);
		content.add(operatePaneTabs);
		content.add(background);
	}

}
