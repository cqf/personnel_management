package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SalaryManagePanel extends JPanel{

	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;
	
	String salaryOperations[]={"查询员工工资", "修改员工工资"};
	
	private static final int SALARYPANEL_WIDTH = 700;
	private static final int SALARYPANEL_HEIGHT = 270;
	
	private JTabbedPane salaryOpPaneTabs = null;
	private SearchSalaryPanel searchSalaryPanel=null;
	private AddSalaryPanel addSalaryPanel=null;
	
	public SalaryManagePanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(30, 10, SALARYPANEL_WIDTH, SALARYPANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
		
		salaryOpPaneTabs=new JTabbedPane();
		searchSalaryPanel=new SearchSalaryPanel(empID,permission,dbm,belongPermission);
		addSalaryPanel=new AddSalaryPanel(empID,permission,dbm,belongPermission);
		
		salaryOpPaneTabs.addTab(salaryOperations[0], searchSalaryPanel);
		salaryOpPaneTabs.addTab(salaryOperations[1], addSalaryPanel);
		
		salaryOpPaneTabs.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if(((JTabbedPane)e.getSource()).getSelectedIndex()==0){
					searchSalaryPanel.updateInfo();
				}
			}
			
		});
			
		
		salaryOpPaneTabs.setBackground(Color.white);
		this.add(salaryOpPaneTabs,BorderLayout.CENTER);
	}
	
}
