package enterpriseManage;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import structs.EmployeeInfo;


public class LoginFrame extends JFrame {
	private static final int WIDTH = 400; // 界面宽度
	private static final int HEIGHT = 500; // 界面高度

	private JLabel titleLabel = null;
	private JLabel userIDLabel = null;
	private JLabel passwordLaebel = null;
	private JLabel userRightLabel = null;

	private JTextField userIDText = null;
	private JPasswordField passwordText = null;

	private JButton startButton = null; // 开始按钮
	private JButton exitButton = null; // 退出按钮

	private ButtonGroup userRightGroup = null;
	private JRadioButton managerButton = null;
	private JRadioButton diManagerButton = null;
	private JRadioButton staffButton = null;

	private DatabaseManage dbm;
	String userID;
	String password;
	int userPermission=Constant.UserPermission.NULL_RIGHT;
	private String[] empInfo;

	public LoginFrame() {
		this.setTitle("人力资源管理系统");
		this.setBounds(300, 50, WIDTH, HEIGHT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);
		
		ImageIcon img = new ImageIcon("src/Images//2012030516553011.jpg");
		JLabel background = new JLabel(img);
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());

		titleLabel = new JLabel("人力资源管理系统");
		titleLabel.setFont(new Font("楷体", Font.BOLD, 22));
		titleLabel.setBounds(50, 50, 300, 50);

		userIDLabel = new JLabel("用户名:");
		userIDLabel.setFont(new Font("楷体", Font.BOLD, 15));
		userIDLabel.setBounds(80, 150, 80, 50);

		passwordLaebel = new JLabel("密码:");
		passwordLaebel.setFont(new Font("楷体", Font.BOLD, 15));
		passwordLaebel.setBounds(80, 200, 80, 50);

		userRightLabel = new JLabel("请选择身份:");
		userRightLabel.setFont(new Font("楷体", Font.BOLD, 15));
		userRightLabel.setBounds(80, 250, 100, 50);

		userIDText = new JTextField();
		userIDText.setBounds(150, 160, 100, 30);

		passwordText = new JPasswordField();
		passwordText.setBounds(150, 210, 100, 30);

		userRightGroup = new ButtonGroup();
		managerButton = new JRadioButton("总经理", false);
		managerButton.setBounds(180, 260, 100, 30);

		diManagerButton = new JRadioButton("部门经理", false);
		diManagerButton.setBounds(180, 300, 100, 30);

		staffButton = new JRadioButton("员工", false);
		staffButton.setBounds(180, 340, 100, 30);

		managerButton.addActionListener(UserRightListener);
		diManagerButton.addActionListener(UserRightListener);
		staffButton.addActionListener(UserRightListener);

		userRightGroup.add(managerButton);
		userRightGroup.add(diManagerButton);
		userRightGroup.add(staffButton);

		startButton = new JButton("登录");
		startButton.setBounds(80, 390, 100, 30);

		exitButton = new JButton("退出");
		exitButton.setBounds(220, 390, 100, 30);
		
		content.add(titleLabel);
		content.add(userIDLabel);
		content.add(passwordLaebel);
		content.add(userRightLabel);
		content.add(userIDText);
		content.add(passwordText);
		content.add(managerButton);
		content.add(diManagerButton);
		content.add(staffButton);
		content.add(startButton);
		content.add(exitButton);
		content.add(background);

		startButton.addActionListener(new MyButtonListener());
		exitButton.addActionListener(new MyButtonListener());
		this.dbm = new DatabaseManage();

	//	String str=dbm.searchDeptIdByDeptName("财务");
	//	System.out.println(str);
	}

	/**
	 * 登录和退出的按钮监听
	 * @author lenovo
	 *
	 */
	public class MyButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			userID = userIDText.getText().trim();
			password = passwordText.getText().trim();
			
			if (e.getActionCommand().equals(startButton.getText())) {
				
				if(userID.equals("")){
					JOptionPane.showMessageDialog(null, "请用自己员工号码登录", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else if(password.equals("")){
					JOptionPane.showMessageDialog(null, "密码不能为空", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else if(userPermission==Constant.UserPermission.NULL_RIGHT){
					JOptionPane.showMessageDialog(null, "请选择身份", "Error",
							JOptionPane.ERROR_MESSAGE);
				}else{
					if(dbm.isLogin(userID, password, userPermission)){
						if(userPermission==Constant.UserPermission.STAFF_RIGHT){
							new StaffFrame(userID,userPermission,dbm).setVisible(true);
						}else {
							new ManagerFrame(userID,userPermission,dbm).setVisible(true);
						}
						
					}else{
						JOptionPane.showMessageDialog(null, "登录失败,用户名/密码/身份错误", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			} else if(e.getActionCommand().equals(exitButton.getText())){
				dbm.disconnect();
				LoginFrame.this.setVisible(false);
				System.exit(0);		
			}
		}
	}

	/**
	 * 身份级别单选按钮监听
	 */
	ActionListener UserRightListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (managerButton.isSelected()) {
				userPermission = Constant.UserPermission.MANAGER_RIGHT;
				//System.out.println(userPermission);
			} else if (diManagerButton.isSelected()) {
				userPermission = Constant.UserPermission.DIVISION_MANAGER_RIGHT;
				//System.out.println(userPermission);
			} else if (staffButton.isSelected()) {
				userPermission = Constant.UserPermission.STAFF_RIGHT;
				//System.out.println(userPermission);
			}
		}
	};

	public static void main(String[] args) {
		new LoginFrame().setVisible(true);
	}

}
