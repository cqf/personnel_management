package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import structs.ChoiceBox;
import structs.ExcelOutput;
import structs.RecordInfo;

public class WorkRecordManagePanel extends JPanel{
	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;
	
	String deptName;
	
	private static final int SEARCH_EMPRECORD_PANEL_WIDTH = 700;
	private static final int SEARCH_EMPRECORD_PANEL_HEIGHT = 270;
	
	String[] TableHeads = { "员工号", "员工姓名","部门名", "日期","签到时间", "签离时间", "备注"};
	
	Object[][] infos={};
	RecordInfo[] recordInfos=null;
	
	String deptID;
	
	
	ChoiceBox empIDBox,dateBOX;
	String[] empinfos,dates;
	
	JPanel headPanel;
	private JButton search_emp_button,search_date_button,search_all_button;
	private JButton print_button;
	RecordInfoTable mt;
	JTable t;
	
	public WorkRecordManagePanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(30, 10, SEARCH_EMPRECORD_PANEL_WIDTH, SEARCH_EMPRECORD_PANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
		
		deptID=dbm.searchDeptIDByempID(empID);
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empinfos=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
			deptName=dbm.searchDeptNameBydeptID(deptID);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empinfos=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		
		empIDBox=new ChoiceBox(empinfos);
		empIDBox.addItem(empID);
		
		dates=dbm.searchWorkRecordDate();
		dateBOX=new ChoiceBox(dates);
		
		headPanel=new JPanel();
		search_emp_button=new JButton("按员工号查询");
		search_date_button=new JButton("按日期查询");
		search_all_button=new JButton("查询全部");
		print_button=new JButton("打印信息");
		
		search_emp_button.addActionListener(new MyButtonListener());
		search_date_button.addActionListener(new MyButtonListener());
		search_all_button.addActionListener(new MyButtonListener());
		print_button.addActionListener(new MyButtonListener());
		
		
		headPanel.add(empIDBox);
		headPanel.add(search_emp_button);
		headPanel.add(dateBOX);
		headPanel.add(search_date_button);
		headPanel.add(search_all_button);
		headPanel.add(print_button);
		
		
		mt = new RecordInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);

		this.add(s, BorderLayout.CENTER);	
		this.add(headPanel,BorderLayout.NORTH);
		
	}
	
	public void update(){
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empinfos=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
			deptName=dbm.searchDeptNameBydeptID(deptID);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empinfos=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		empIDBox.setInfos(empinfos);
		
		dates=dbm.searchWorkRecordDate();
		dateBOX.setInfos(dates);
		
		empIDBox.updateUI();
		dateBOX.updateUI();
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==search_emp_button){
				String empID=(String)empIDBox.getSelectedItem();
				recordInfos=dbm.searchEmpWorkRecordByEmpID(empID);
				int length=recordInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=recordInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=recordInfos[i].getDeptID();
					infos[i][3]=recordInfos[i].getDay();
					infos[i][4]=recordInfos[i].getSignIntime();
					infos[i][5]=recordInfos[i].getSignOutTime();
					infos[i][6]=recordInfos[i].getState();
				}
				mt.setInfos(infos);
				t.updateUI();			
			}else if(e.getSource()==search_date_button){
				String date=(String)dateBOX.getSelectedItem();
				if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
					recordInfos=dbm.searchEmpWorkRecordByDate(deptID,date,belongPermission);
				}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
					recordInfos=dbm.searchDiEmpWorkRecordByDate(date, belongPermission);
				}
				
				int length=recordInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=recordInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=recordInfos[i].getDeptID();
					infos[i][3]=recordInfos[i].getDay();
					infos[i][4]=recordInfos[i].getSignIntime();
					infos[i][5]=recordInfos[i].getSignOutTime();
					infos[i][6]=recordInfos[i].getState();
				}
				mt.setInfos(infos);
				t.updateUI();		
			}else if(e.getSource()==search_all_button){
				if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
					recordInfos=dbm.searchAllEmpWorkRecordByDeptID(deptID,belongPermission);
				}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
					recordInfos=dbm.searchAllDiEmpWorkRecordByPermission(belongPermission);
				}
				
				int length=recordInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=recordInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=recordInfos[i].getDeptID();
					infos[i][3]=recordInfos[i].getDay();
					infos[i][4]=recordInfos[i].getSignIntime();
					infos[i][5]=recordInfos[i].getSignOutTime();
					infos[i][6]=recordInfos[i].getState();
				}
				mt.setInfos(infos);
				t.updateUI();	
			}else if(e.getSource()==print_button){
				ExcelOutput out=new ExcelOutput("员工考勤表",TableHeads);
				out.printExcel(infos);
			}
		}	
	}
	
	class RecordInfoTable extends AbstractTableModel {

		Object[][] infos;
		
		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public RecordInfoTable(Object[][] infos){
			this.infos=infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}
	}
}
