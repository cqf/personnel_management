package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import structs.ChoiceBox;
import structs.EmployeeInfo;
import structs.ExcelOutput;

public class SearchEmpInfoPanel extends JPanel {

	private String empID = null;
	private int permission = Constant.UserPermission.NULL_RIGHT;
	DatabaseManage dbm;
	private String belongPermission;

	EmployeeInfo[] empInfos = null;
	Object[][] infos = {};

	String deptID;
	String deptName;

	private JPanel headPanel;
	ChoiceBox empIDBox;
	String[] empIDs;
	JButton search_empID_Button, search_all_button;
	private JButton print_button;

	String[] TableHeads = { "员工号", "员工姓名", "所属部门", "入职时间", "性别", "生日", "身份证号",
			"年龄", "政治面貌", "学历", "联系电话" };

	private static final int SEARCHEMPINFOPANEL_WIDTH = 800;
	private static final int SEARCHEMPINFOPANEL_HEIGHT = 100;

	EmpInfoTable mt;
	JTable t;

	public SearchEmpInfoPanel(String empID, int permission, DatabaseManage dbm,
			String belongPermission) {
		this.setBounds(50, 90, SEARCHEMPINFOPANEL_WIDTH,
				SEARCHEMPINFOPANEL_HEIGHT);
		this.setLayout(new BorderLayout());

		this.empID = empID;
		this.permission = permission;
		this.dbm = dbm;
		this.belongPermission = belongPermission;

		headPanel = new JPanel();
		deptID = dbm.searchDeptIDByempID(empID);

		if (permission == Constant.UserPermission.DIVISION_MANAGER_RIGHT) {
			empIDs = dbm.searchAllEmpIDBydeptID(deptID, belongPermission);
			deptName = dbm.searchDeptNameBydeptID(deptID);
		} else if (permission == Constant.UserPermission.MANAGER_RIGHT) {
			empIDs = dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		empIDBox = new ChoiceBox(empIDs);
		empIDBox.addItem(empID);

		search_empID_Button = new JButton("按员工号查询");
		search_all_button = new JButton("查询全部员工");

		mt = new EmpInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);	

		print_button=new JButton("打印信息");
		
		search_empID_Button.addActionListener(new MyButtonListener());
		search_all_button.addActionListener(new MyButtonListener());
		print_button.addActionListener(new MyButtonListener());
		
		headPanel.add(empIDBox);
		headPanel.add(search_empID_Button);
		headPanel.add(search_all_button);
		headPanel.add(print_button);
		s.setBackground(Color.white);
		this.setBackground(Color.white);
		this.add(headPanel, BorderLayout.NORTH);
		this.add(s, BorderLayout.CENTER);
	}

	public void updateDBInfo() {
		if (permission == Constant.UserPermission.DIVISION_MANAGER_RIGHT) {
			empIDs = dbm.searchAllEmpIDBydeptID(deptID, belongPermission);
		} else if (permission == Constant.UserPermission.MANAGER_RIGHT) {
			empIDs = dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		empIDBox.setInfos(empIDs);
		empIDBox.updateUI();
	}

	public class MyButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == search_empID_Button) {
				String empID = (String) empIDBox.getSelectedItem();
				empInfos = dbm.searchEmpInfoByEmpID(empID);
				int length = empInfos.length;
				infos = new Object[length][TableHeads.length];
				for (int i = 0; i < length; i++) {
					System.out.println(empInfos[i]);
					infos[i][0] = empInfos[i].getEmpID();
					infos[i][1] = empInfos[i].getEmpName();
					infos[i][2] = empInfos[i].getDept();
					infos[i][3] = empInfos[i].getHireTime();
					infos[i][4] = empInfos[i].getGender();
					infos[i][5] = empInfos[i].getBirthday();
					infos[i][6] = empInfos[i].getIDcard();
					infos[i][7] = empInfos[i].getAge();
					infos[i][8] = empInfos[i].getPoliticalParty();
					infos[i][9] = empInfos[i].getEducation();
					infos[i][10] = empInfos[i].getTelephone();
				}
				mt.setInfos(infos);
				t.updateUI();

			} else if (e.getSource() == search_all_button) {
				if (permission == Constant.UserPermission.DIVISION_MANAGER_RIGHT) {
					empInfos = dbm.searchAllEmpInfoByDeptID(deptID);
				} else if (permission == Constant.UserPermission.MANAGER_RIGHT) {
					empInfos = dbm.searchAllDiEmpInfoByPermission(belongPermission);
				}
				int length = empInfos.length;
				infos = new Object[length][TableHeads.length];

				for (int i = 0; i < length; i++) {
					infos[i][0] = empInfos[i].getEmpID();
					infos[i][1] = empInfos[i].getEmpName();
					infos[i][2] = empInfos[i].getDept();
					infos[i][3] = empInfos[i].getHireTime();
					infos[i][4] = empInfos[i].getGender();
					infos[i][5] = empInfos[i].getBirthday();
					infos[i][6] = empInfos[i].getIDcard();
					infos[i][7] = empInfos[i].getAge();
					infos[i][8] = empInfos[i].getPoliticalParty();
					infos[i][9] = empInfos[i].getEducation();
					infos[i][10] = empInfos[i].getTelephone();
				}
				mt.setInfos(infos);
				t.updateUI();
			}else if(e.getSource()==print_button){
				ExcelOutput out=new ExcelOutput("员工人事信息表",TableHeads);
				out.printExcel(infos);
			}
		}

	}

	class EmpInfoTable extends AbstractTableModel {

		Object[][] infos;

		public EmpInfoTable(Object[][] infos) {
			this.infos = infos;
		}

		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}

		@Override
		public void fireTableChanged(TableModelEvent arg0) {
			super.fireTableChanged(arg0);
		}
	}
}
