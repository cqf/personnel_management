package enterpriseManage;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import structs.ChoiceBox;
import structs.SalaryInfo;

public class AddSalaryPanel extends JPanel {
	private String empID = null;
	private int permission = Constant.UserPermission.NULL_RIGHT;
	DatabaseManage dbm;
	private String belongPermission;

	private static final int PANEL_WIDTH = 800;
	private static final int PANEL_HEIGHT = 300;

	private JLabel[] addEmpInfoTabLabel = { new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel() };
	private static final String[] addEmpInfoTab = { "员工号", "所属部门", "年份", "月份",
			"基本工资", "奖金", "罚款" };

	private JTextField[] addEmpInfoText = { new JTextField(), new JTextField(),
			new JTextField(), new JTextField(), new JTextField(),
			new JTextField(), new JTextField() };
	
	private JPanel addInfoLabelPanel1;
	private JPanel addInfoLabelPanel2;
	private ChoiceBox empIDBox, deptNameBox,yearBox,monthBox;
	private String[] empIDs, deptNames,years;
	private String[] months={"01","02","03","04","05","06","07","08","09","10","11","12"};
	private JButton addButton;
	
	String deptID;

	public AddSalaryPanel(String empID, int permission, DatabaseManage dbm,String belongPermission) {
		this.setBounds(50, 10, PANEL_WIDTH, PANEL_HEIGHT);
		this.setLayout(null);

		this.empID = empID;
		this.permission = permission;
		this.dbm = dbm;
		this.belongPermission=belongPermission;
		
		deptID = dbm.searchDeptIDByempID(empID);
		if (permission == Constant.UserPermission.DIVISION_MANAGER_RIGHT) {
			empIDs = dbm.searchAllEmpIDBydeptID(deptID, belongPermission);
			deptNames=new String[1];
			deptNames[0]=dbm.searchDeptNameBydeptID(deptID);
		} else if (permission == Constant.UserPermission.MANAGER_RIGHT) {
			empIDs = dbm.searchAllDiEmpIDByPermission(belongPermission);
			deptNames=dbm.searchAllDeptName();
		}
		empIDBox = new ChoiceBox(empIDs);
		empIDBox.addItem(empID);
		
		deptNameBox = new ChoiceBox(deptNames);
		
		Calendar cal=Calendar.getInstance();
		years=new String[1];
		years[0]=String.valueOf(cal.get(Calendar.YEAR));
		yearBox=new ChoiceBox(years);
		
		monthBox=new ChoiceBox(months);

		initInfo();

		
		this.add(addInfoLabelPanel1);
		this.add(addInfoLabelPanel2);
		this.add(addButton);

	}
	public void initInfo() {

		addInfoLabelPanel1 = new JPanel();
		addInfoLabelPanel1.setBounds(0, 50, 700, 30);
		addInfoLabelPanel1.setLayout(new GridLayout(1, 8, 5, 100));
		
		addInfoLabelPanel2=new JPanel();
		addInfoLabelPanel2.setBounds(0,130,700,30);
		addInfoLabelPanel2.setLayout(new GridLayout(1, 8, 10, 100));

		for (int i = 0; i < 7; i++) {
			addEmpInfoTabLabel[i].setText(addEmpInfoTab[i]);
		}
		for (int i = 0; i < 7; i++) {
			addEmpInfoText[i].setSize(50, 10);
		}
		
		addInfoLabelPanel1.add(addEmpInfoTabLabel[0]);
		addInfoLabelPanel1.add(empIDBox);
		
		addInfoLabelPanel1.add(addEmpInfoTabLabel[1]);
		addInfoLabelPanel1.add(deptNameBox);
		
		addInfoLabelPanel1.add(addEmpInfoTabLabel[2]);
		addInfoLabelPanel1.add(yearBox);
		
		addInfoLabelPanel1.add(addEmpInfoTabLabel[3]);
		addInfoLabelPanel1.add(monthBox);

		for (int i = 4; i < 7; i++) {
			addInfoLabelPanel2.add(addEmpInfoTabLabel[i]);
			addInfoLabelPanel2.add(addEmpInfoText[i]);
		}

		addButton = new JButton("添加");
		addButton.setBounds(330, 200, 100, 30);
		addButton.addActionListener(new MyButtonListener());
	}
	
	public class MyButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == addButton) {
				int i = JOptionPane.showConfirmDialog(null, "确认添加该员工工资信息", "提示",
						JOptionPane.YES_NO_OPTION);
				if(i==JOptionPane.YES_OPTION){
					String newEmpID=String.valueOf(empIDBox.getSelectedItem());
					String newDeptID=dbm.searchDeptIdByDeptName(String.valueOf(deptNameBox.getSelectedItem()));
					String newYear=String.valueOf(yearBox.getSelectedItem());
					String newMonth=String.valueOf(monthBox.getSelectedItem());
					String newBasic=addEmpInfoText[4].getText();
					String newBonus=addEmpInfoText[5].getText();
					String newFine=addEmpInfoText[6].getText();
					int newSum=Integer.parseInt(newBasic)+Integer.parseInt(newBonus)-Integer.parseInt(newFine);
					
					SalaryInfo salary=new SalaryInfo();
					salary.setEmpID(newEmpID);
					salary.setDeptID(newDeptID);
					salary.setYear(newYear);
					salary.setMonth(newMonth);
					salary.setBasicSalary(newBasic);
					salary.setBonus(newBonus);
					salary.setFine(newFine);
					salary.setSumSalary(String.valueOf(newSum));
				//	System.out.println(salary.getEmpID()+" "+salary.getDeptID()+" "+salary.getYear()+" "+salary.getMonth()+" "+salary.getBasicSalary()+" "+salary.getBonus()+" "+salary.getFine()+" "+salary.getSumSalary());
					boolean b=dbm.addNewSalaryInfoToDB(salary);
					
					if(b){
						JOptionPane.showMessageDialog(null, "添加成功", "Infomation",
								JOptionPane.INFORMATION_MESSAGE);		
					}else{
						JOptionPane.showMessageDialog(null, "添加失败", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}else if(i==JOptionPane.NO_OPTION){
					
				}
				
			}
		}

	}
	
	
}
