package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import structs.ChoiceBox;
import structs.EmployeeInfo;

public class DeleteEmpInfoPanel extends JPanel{

	private String empID=null;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	DatabaseManage dbm;
	private String belongPermission;
	
	private static final int DELETE_EMPINFOPANEL_WIDTH = 800;
	private static final int DELETE_EMPINFOPANEL_HEIGHT = 100;
	
	private JPanel headPanel;
	ChoiceBox empID_box;
	JButton search_emp_button;
	JButton delete_emp_button;
	EmployeeInfo[] empInfos=null;
	Object[][] infos={};
	String[] empIDs;
	String deptID;
	
	String[] TableHeads = { "员工号", "员工姓名", "所属部门", "入职时间", "性别", "生日",
			"身份证号", "年龄", "政治面貌", "学历", "联系电话" };
	
	EmpInfoTable mt;
	JTable t ;
	
	public DeleteEmpInfoPanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(50, 90, DELETE_EMPINFOPANEL_WIDTH,
				DELETE_EMPINFOPANEL_HEIGHT);
		this.setLayout(new BorderLayout());

		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
		
		deptID=dbm.searchDeptIDByempID(empID);
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empIDs=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empIDs=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
			
		empID_box=new ChoiceBox(empIDs);
		//empID_box.addItem(empID);
		
		mt = new EmpInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);
		
		
		headPanel=new JPanel();
		search_emp_button=new JButton("查询");
		delete_emp_button=new JButton("删除该员工人事信息");
		
		search_emp_button.addActionListener(new MyButtonListener());
		delete_emp_button.addActionListener(new MyButtonListener());
		
		headPanel.add(empID_box);
		headPanel.add(search_emp_button);
		headPanel.add(delete_emp_button);
		
		s.setBackground(Color.white);
		this.setBackground(Color.white);
		this.add(headPanel,BorderLayout.NORTH);
		this.add(s,BorderLayout.CENTER);
	}
	
	public void updateDBInfo(){
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empIDs=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empIDs=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		
		for(int i=0;i<empIDs.length;i++){
			System.out.println(empIDs[i]);
		}
		
		
		empID_box.setInfos(empIDs);
		//empID_box.addItem(empID);
		//System.out.println("oiuoiuoiu");
		empID_box.updateUI();
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==search_emp_button){
				String empID=(String)empID_box.getSelectedItem();
				empInfos=dbm.searchEmpInfoByEmpID(empID);
				int length=empInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=empInfos[i].getEmpID();
					infos[i][1]=empInfos[i].getEmpName();
					infos[i][2]=empInfos[i].getDept();
					infos[i][3]=empInfos[i].getHireTime();
					infos[i][4]=empInfos[i].getGender();
					infos[i][5]=empInfos[i].getBirthday();
					infos[i][6]=empInfos[i].getIDcard();
					infos[i][7]=empInfos[i].getAge();
					infos[i][8]=empInfos[i].getPoliticalParty();
					infos[i][9]=empInfos[i].getEducation();
					infos[i][10]=empInfos[i].getTelephone();
				}
				mt.setInfos(infos);
				t.updateUI();
			}else if(e.getSource()==delete_emp_button){
				String empID=(String)empID_box.getSelectedItem();
			
				boolean ub=dbm.deleteUserByUserID(empID);
				boolean eb=dbm.deleteEmpInfoByEmpID(empID);
				boolean sb=dbm.deleteSalaryByEmpID(empID);
				boolean wb=dbm.deleteWorkRecordByEmpID(empID);
				
				empID_box.removeItem(empID);
System.out.println(ub+" "+eb+" "+sb+" "+wb);
				if(ub&&sb&&wb){
					
				}
			}
		}
		
	}
	
	class EmpInfoTable extends AbstractTableModel {

		Object[][] infos;
		
		public EmpInfoTable(Object[][] infos){
			this.infos=infos;
		}

		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}
	}
	
}
