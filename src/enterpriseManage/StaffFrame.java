package enterpriseManage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class StaffFrame extends JFrame{
	String userID;
	int permission;
	DatabaseManage dbm;
	
	private String[] operations = {"查询工资", "查询考勤"};
	
	private static final int WIDTH = 900; // 界面宽度
	private static final int HEIGHT = 600; // 界面高度
	private static final int OPERATEPANEL_WIDTH = 800;
	private static final int OPERATEPANEL_HEIGHT = 320;
	
	private JLabel titleLabel = null;

	private EmpInfoPanel empInfoPanel = null;
	private JTabbedPane operatePaneTabs = null;

	private StaffSalaryPanel salaryPanel=null;
	private StaffWorkRecordPanel recordPanel=null;
	
	public StaffFrame(String userID, int permission, DatabaseManage dbm){
		this.userID = userID;
		this.permission = permission;
		this.dbm = dbm;
		
		this.setTitle("人力资源管理系统");
		this.setBounds(300, 50, WIDTH, HEIGHT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);
		
		ImageIcon img = new ImageIcon("src/Images//25.jpg");
		JLabel background = new JLabel(img);
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		
		titleLabel = new JLabel("人力资源管理系统");
		titleLabel.setFont(new Font("楷体", Font.BOLD, 22));
		titleLabel.setBounds(300, 20, 300, 50);
		
		empInfoPanel = new EmpInfoPanel(userID, permission, dbm);
		
		operatePaneTabs = new JTabbedPane();
		operatePaneTabs.setBounds(50, 220, OPERATEPANEL_WIDTH,
				OPERATEPANEL_HEIGHT);
		
		
		salaryPanel=new StaffSalaryPanel(userID,permission,dbm);
		operatePaneTabs.addTab(operations[0], salaryPanel);
		
		recordPanel=new StaffWorkRecordPanel(userID, permission, dbm);
		operatePaneTabs.addTab(operations[1], recordPanel);
		
		operatePaneTabs.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if (((JTabbedPane) e.getSource()).getSelectedIndex() == 0) {
					salaryPanel.updateInfo();
				} else if (((JTabbedPane) e.getSource()).getSelectedIndex() == 1) {
					recordPanel.update();
				}
			}

		});
		operatePaneTabs.setBackground(Color.white);
		content.add(titleLabel);
		content.add(empInfoPanel);
		content.add(operatePaneTabs);
		content.add(background);
		
	}
	
	
}
