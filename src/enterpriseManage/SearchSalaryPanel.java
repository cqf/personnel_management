package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import structs.ChoiceBox;
import structs.ExcelOutput;
import structs.SalaryInfo;

public class SearchSalaryPanel extends JPanel{

	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;
	
	private static final int SALARY_SEARCHPANEL_WIDTH = 800;
	private static final int SALARY_SEARCHPANEL_HEIGHT = 270;
	
	String[] TableHeads = { "员工号","员工姓名", "部门名", "年份", "月份","基本工资", "奖金", "罚款",
			"工资总和"};
	Object[][] infos={};
	SalaryInfo[] salaryInfos=null;
	
	ChoiceBox empIDBox,yearBox,monthBox;
	String[] empIDs,years,months;
	
	private JPanel headPanel;
	private JButton print_button;
	private JButton search_emp_button,search_date_button,search_all_button;
	SalaryInfoTable mt;
	JTable t;
	
	String deptID;
	String deptName;
	
	public SearchSalaryPanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(30, 10, SALARY_SEARCHPANEL_WIDTH, SALARY_SEARCHPANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
		
		deptID=dbm.searchDeptIDByempID(empID);
		
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empIDs=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
			deptName=dbm.searchDeptNameBydeptID(deptID);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empIDs=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		
		empIDBox=new ChoiceBox(empIDs);
		empIDBox.addItem(empID);
		
		years=dbm.searchSalaryYear();
		yearBox=new ChoiceBox(years);
		
		months=dbm.searchSalaryMonth();
		monthBox=new ChoiceBox(months);
			
		print_button=new JButton("打印信息");
		headPanel=new JPanel();
		search_emp_button=new JButton("按员工号查询");
		search_date_button=new JButton("按日期查询");
		search_all_button=new JButton("查询全部");
		
		search_emp_button.addActionListener(new MyButtonListener());
		search_date_button.addActionListener(new MyButtonListener());
		search_all_button.addActionListener(new MyButtonListener());
		print_button.addActionListener(new MyButtonListener());
		
		headPanel.add(empIDBox);
		headPanel.add(search_emp_button);
		headPanel.add(yearBox);
		headPanel.add(monthBox);
		headPanel.add(search_date_button);
		headPanel.add(search_all_button);
		headPanel.add(print_button);
		
		mt = new SalaryInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);
		
		s.setBackground(Color.white);
		this.setBackground(Color.white);
		this.add(headPanel,BorderLayout.NORTH);
		this.add(s, BorderLayout.CENTER);
	}
	
	public void updateInfo(){
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empIDs=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empIDs=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		empIDBox.setInfos(empIDs);
		
		years=dbm.searchSalaryYear();
		yearBox.setInfos(years);
		
		months=dbm.searchSalaryMonth();
		monthBox.setInfos(months);
		
		empIDBox.updateUI();
		yearBox.updateUI();
		monthBox.updateUI();
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==search_emp_button){
				String empID=(String)empIDBox.getSelectedItem();
				salaryInfos=dbm.searchEmpSalaryByEmpID(empID);
				int length=salaryInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=salaryInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=salaryInfos[i].getDeptID();
					infos[i][3]=salaryInfos[i].getYear();
					infos[i][4]=salaryInfos[i].getMonth();
					infos[i][5]=salaryInfos[i].getBasicSalary();
					infos[i][6]=salaryInfos[i].getBonus();
					infos[i][7]=salaryInfos[i].getFine();
					infos[i][8]=salaryInfos[i].getSumSalary();			
				}
				mt.setInfos(infos);
				t.updateUI();
			}else if(e.getSource()==search_date_button){
				String year=(String)yearBox.getSelectedItem();
				String month=(String)monthBox.getSelectedItem();
				if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
					salaryInfos=dbm.searchEmpSalaryByDate(deptID,year, month,belongPermission);
				}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
					salaryInfos=dbm.searchDiEmpSalaryByDate(year, month, belongPermission);
				}
				
				int length=salaryInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=salaryInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=salaryInfos[i].getDeptID();
					infos[i][3]=salaryInfos[i].getYear();
					infos[i][4]=salaryInfos[i].getMonth();
					infos[i][5]=salaryInfos[i].getBasicSalary();
					infos[i][6]=salaryInfos[i].getBonus();
					infos[i][7]=salaryInfos[i].getFine();
					infos[i][8]=salaryInfos[i].getSumSalary();		
				}
				mt.setInfos(infos);
				t.updateUI();
			}else if(e.getSource()==search_all_button){
				if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
					salaryInfos=dbm.searchAllSalaryInfoByDeptID(deptID,belongPermission);
				}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
					System.out.println(belongPermission);
					salaryInfos=dbm.searchAllDiSalaryInfoByPermission(belongPermission);
				}
				
				int length=salaryInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=salaryInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=salaryInfos[i].getDeptID();
					infos[i][3]=salaryInfos[i].getYear();
					infos[i][4]=salaryInfos[i].getMonth();
					infos[i][5]=salaryInfos[i].getBasicSalary();
					infos[i][6]=salaryInfos[i].getBonus();
					infos[i][7]=salaryInfos[i].getFine();
					infos[i][8]=salaryInfos[i].getSumSalary();		
				}
				mt.setInfos(infos);
				t.updateUI();
			}else if(e.getSource()==print_button){
				ExcelOutput out=new ExcelOutput("员工工资表",TableHeads);
				out.printExcel(infos);
			}
		}
		
	}
	
	
	class SalaryInfoTable extends AbstractTableModel {

		Object[][] infos;
		
		public SalaryInfoTable(Object[][] infos){
			this.infos=infos;
		}

		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}

		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}
	}
	
}
