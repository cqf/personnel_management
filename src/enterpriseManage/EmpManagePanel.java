package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class EmpManagePanel extends JPanel {
	
	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;
	
	private String[] operations = { "查询员工信息", "添加员工信息", "删除员工信息"};
	
	private static final int EMP_MANAGE_PANEL_WIDTH = 800;
	private static final int EMP_MANAGE_PANEL_HEIGHT = 300;
	
	private JTabbedPane EmpManagePaneTabs = null;
	private SearchEmpInfoPanel searchPanel=null;
	private AddEmpInfPanel addPanel=null;
	private DeleteEmpInfoPanel deletePanel=null;
	private AddSalaryPanel updatePanel=null;
	
	public EmpManagePanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(30, 10, EMP_MANAGE_PANEL_WIDTH, EMP_MANAGE_PANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
			
		EmpManagePaneTabs=new JTabbedPane();
		searchPanel=new SearchEmpInfoPanel(empID,permission,dbm,belongPermission);
		EmpManagePaneTabs.addTab(operations[0], searchPanel);
		
		addPanel=new AddEmpInfPanel(empID,permission,dbm,belongPermission);
		EmpManagePaneTabs.addTab(operations[1], addPanel);
		
		deletePanel=new DeleteEmpInfoPanel(empID,permission,dbm,belongPermission);
		EmpManagePaneTabs.addTab(operations[2], deletePanel);
	
		EmpManagePaneTabs.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if(((JTabbedPane)e.getSource()).getSelectedIndex()==0){
					searchPanel.updateDBInfo();
				}else if(((JTabbedPane)e.getSource()).getSelectedIndex()==2){
					System.out.println("eeeeeeeeee");
					deletePanel.updateDBInfo();
				}
			}		
		});
		
		EmpManagePaneTabs.setBackground(Color.white);
		this.add(EmpManagePaneTabs,BorderLayout.CENTER);
	
	}
}
