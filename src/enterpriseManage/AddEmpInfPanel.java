package enterpriseManage;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import structs.ChoiceBox;
import structs.DateChooser;
import structs.EmployeeInfo;

public class AddEmpInfPanel extends JPanel {

	private String empID;
	private int permission = Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;

	private static final int PANEL_WIDTH = 800;
	private static final int PANEL_HEIGHT = 300;

	private JLabel[] addEmpInfoTabLabel = { new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel(), new JLabel() };
	private static final String[] addEmpInfoTab = { "员工号", "员工姓名", "所属部门",
			"入职时间", "性别", "生日", "身份证号", "年龄", "政治面貌", "学历", "联系电话", "职位", };

	private JTextField[] addEmpInfoText = { new JTextField(), new JTextField(),
			new JTextField(), new JTextField(), new JTextField(),
			new JTextField(), new JTextField(), new JTextField(),
			new JTextField(), new JTextField(), new JTextField(),
			new JTextField() };

	private JPanel addInfoLabelPanel;
	private DateChooser timeChooser;
	private ChoiceBox deptBox, dutyBox;
	private String[] deptNames, duties;
	private String deptState = null;
	int permissionState = Constant.UserPermission.NULL_RIGHT;
	private JButton addButton;
	
	String deptID;

	public AddEmpInfPanel(String empID, int permission, DatabaseManage dbm,String belongPermission) {
		this.setBounds(50, 10, PANEL_WIDTH, PANEL_HEIGHT);
		this.setLayout(null);

		this.empID = empID;
		this.permission = permission;
		this.dbm = dbm;
		this.belongPermission=belongPermission;
		
		getInfoFromDB();

		initInfo();
		
		this.add(addInfoLabelPanel);
		this.add(addButton);
	}

	public void initInfo() {

		addInfoLabelPanel = new JPanel();
		addInfoLabelPanel.setBounds(0, 0, 700, 200);
		addInfoLabelPanel.setLayout(new GridLayout(3, 8, 10, 60));

		for (int i = 0; i < 12; i++) {
			addEmpInfoTabLabel[i].setText(addEmpInfoTab[i]);
		}
		for (int i = 0; i < 12; i++) {
			addEmpInfoText[i].setSize(50, 10);
		}

		timeChooser = new DateChooser();
		deptBox = new ChoiceBox(deptNames);
		dutyBox = new ChoiceBox(duties);

		for (int i = 0; i < 2; i++) {
			addInfoLabelPanel.add(addEmpInfoTabLabel[i]);
			addInfoLabelPanel.add(addEmpInfoText[i]);
		}
		addInfoLabelPanel.add(addEmpInfoTabLabel[2]);
		addInfoLabelPanel.add(deptBox);

		addInfoLabelPanel.add(addEmpInfoTabLabel[3]);
		addInfoLabelPanel.add(timeChooser);

		for (int i = 4; i < 11; i++) {
			addInfoLabelPanel.add(addEmpInfoTabLabel[i]);
			addInfoLabelPanel.add(addEmpInfoText[i]);
		}
		addInfoLabelPanel.add(addEmpInfoTabLabel[11]);
		addInfoLabelPanel.add(dutyBox);

		addButton = new JButton("添加");
		addButton.setBounds(330, 200, 100, 30);
		addButton.addActionListener(new MyButtonListener());
	}

	public void getInfoFromDB() {
		//deptNames = dbm.searchAllDeptName();
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			deptID=dbm.searchDeptIDByempID(empID);
			deptNames=new String[1];
			deptNames[0]=dbm.searchDeptNameBydeptID(deptID);
			duties = new String[2];
			duties[0] = "official";
			duties[1]="temporary";
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			deptID=dbm.searchDeptIDByempID(empID);
			deptNames = dbm.searchAllDeptName();
			duties = new String[1];
			duties[0] = "dept_manager";
		}
		
	}

	public class MyButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == addButton) {
				int i = JOptionPane.showConfirmDialog(null, "确认添加该员工信息", "提示",
						JOptionPane.YES_NO_OPTION);
				if(i==JOptionPane.YES_OPTION){
					int newEmpPermission = Constant.UserPermission.NULL_RIGHT;
					String dutyName = String.valueOf(dutyBox.getSelectedItem());
					if(dutyName.equals("dept_manager")){
						newEmpPermission = Constant.UserPermission.DIVISION_MANAGER_RIGHT;
					}else {
						newEmpPermission = Constant.UserPermission.STAFF_RIGHT;
					}
					EmployeeInfo emp = new EmployeeInfo();
					emp.setEmpID(addEmpInfoText[0].getText());
					emp.setEmpName(addEmpInfoText[1].getText());
					
					emp.setDept(dbm.searchDeptIdByDeptName(String.valueOf(deptBox
							.getSelectedItem())));
					//System.out.println(dbm.searchDeptIdByDeptName(String.valueOf(deptBox.getSelectedItem())));
					//System.out.println(timeChooser.getChooseTime());
					Date now = new Date(); 
				    Calendar cal = Calendar.getInstance();
				    DateFormat d1 = DateFormat.getDateInstance();
					String str1 = d1.format(now);
					//System.out.println("用DateFormat.getDateInstance()格式化时间后为：" + str1);
					emp.setHireTime(str1);
					emp.setGender(addEmpInfoText[4].getText());
					emp.setBirthday(addEmpInfoText[5].getText());
					emp.setIDcard(addEmpInfoText[6].getText());
					emp.setAge(addEmpInfoText[7].getText());
					emp.setPoliticalParty(addEmpInfoText[8].getText());
					emp.setEducation(addEmpInfoText[9].getText());
					emp.setTelephone(addEmpInfoText[10].getText());
					emp.setPermission(newEmpPermission);
					emp.setPassword(Constant.INIT_PASSWORD);
					emp.setDutyID(dbm.searchDutyIdByDutyName(dutyName));
					System.out.println("why"+dbm.searchDutyIdByDutyName(dutyName));
					boolean ub=dbm.addNewUsrToDb(emp);
					System.out.println(ub);
					boolean eb=dbm.addNewEmpInfoToDB(emp);
					System.out.println(eb);
					boolean db=dbm.addNewWorkInfoToDB(emp);
					System.out.println(db);
					if(ub&&eb&&db){
						JOptionPane.showMessageDialog(null, "添加成功", "Infomation",
								JOptionPane.INFORMATION_MESSAGE);		
					}else{
						dbm.deleteEmpInfoByEmpID(emp.getEmpID());
						dbm.deleteUserByUserID(emp.getEmpID());
						dbm.deleteWorkByEmpID(emp.getEmpID());
						JOptionPane.showMessageDialog(null, "添加失败", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}else if(i==JOptionPane.NO_OPTION){
					
				}
				
			}
		}

	}

}
