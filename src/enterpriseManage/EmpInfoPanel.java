package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import structs.EmployeeInfo;

public class EmpInfoPanel extends JPanel {
	
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private String empID=null;
	private String[] empInfo;
	private DatabaseManage dbm;

	private static final int EMPINFOPANEL_WIDTH = 800;
	private static final int EMPINFOPANEL_HEIGHT = 100;
	
	private static final int INFOPANEL_WIDTH=700;
	private static final int INFOPANEL_HEIGHT = 100;
	
	private static final int OPERATEPANEL_WIDTH=100;
	private static final int OPERATEPANEL_HEIGHT = 90;

	private JPanel infoPanel;
	private JPanel operatePanel;
	private JButton updatePassword_button;
	private JButton signIn_button;
	private JButton signOut_button;
	
	String deptID;
	
	int year,month,day,hour,minute,second;
	
	private JLabel[] empInfoTabLabel = { new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel(),new JLabel() };
	private static final String[] empInfoTab = { "员工号", "员工姓名", "所属部门", "入职时间",
			"性别", "生日", "身份证号", "年龄", "政治面貌", "学历", "联系电话","职位" };

	private JLabel[] empInfoLabel = { new JLabel(), new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),
			new JLabel(), new JLabel(), new JLabel(), new JLabel(),new JLabel() };

	public EmpInfoPanel(String empID,int permission,DatabaseManage dbm) {
		this.setBounds(50, 90, EMPINFOPANEL_WIDTH, EMPINFOPANEL_HEIGHT);
		this.setLayout(null);
		
		this.permission=permission;
		this.empID=empID;
		this.dbm=dbm;
		ImageIcon img = new ImageIcon("src/Images//界面背景.jpg");
		JLabel background = new JLabel(img);
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		
		deptID=dbm.searchDeptIDByempID(empID);
		EmployeeInfo emp=new EmployeeInfo();
		emp=dbm.searchEmpInfoByUserID(empID);
		empInfo=new String[12];
		empInfo[0]=emp.getEmpID();
		empInfo[1]=emp.getEmpName();
		empInfo[2]=emp.getDept();
		empInfo[3]=emp.getHireTime();
		empInfo[4]=emp.getGender();
		empInfo[5]=emp.getBirthday();
		empInfo[6]=emp.getIDcard();
		empInfo[7]=emp.getAge();
		empInfo[8]=emp.getPoliticalParty();
		empInfo[9]=emp.getEducation();
		empInfo[10]=emp.getTelephone();
		empInfo[11]=dbm.searchDutyNameByEmpIDDeptID(empID, deptID);
		
		infoPanel=new JPanel();
		infoPanel.setSize(INFOPANEL_WIDTH,INFOPANEL_HEIGHT);
		infoPanel.setLayout(new GridLayout(3, 8, 5, 5));
		for (int i = 0; i < 12; i++) {
			empInfoTabLabel[i].setText(empInfoTab[i]);
		}
		
		for (int i = 0; i < 12; i++) {
			empInfoLabel[i].setText(empInfo[i]);
		}
		for (int i = 0; i < 12; i++) {
			infoPanel.add(empInfoTabLabel[i]);
			infoPanel.add(empInfoLabel[i]);
		}
		
		operatePanel=new JPanel();
		operatePanel.setBounds(700,0,OPERATEPANEL_WIDTH,OPERATEPANEL_HEIGHT);
		operatePanel.setLayout(new BorderLayout());
		
		updatePassword_button=new JButton("修改密码");
		updatePassword_button.setSize(100,30);
		signIn_button=new JButton("签到");
		signIn_button.setSize(100,30);
		signOut_button=new JButton("签出");
		signOut_button.setSize(100,30);
		signOut_button.setEnabled(false);
		
		updatePassword_button.addActionListener(new MyButtonListener());
		signIn_button.addActionListener(new MyButtonListener());
		signOut_button.addActionListener(new MyButtonListener());
		
		operatePanel.add(updatePassword_button,BorderLayout.NORTH);
		operatePanel.add(signIn_button,BorderLayout.CENTER);
		operatePanel.add(signOut_button,BorderLayout.SOUTH);
		
		this.setBackground(Color.white);
		infoPanel.setBackground(Color.white);
		this.add(infoPanel);
		this.add(operatePanel);
		
	}
	
	public void getCurrentTime(){
		Calendar cal=Calendar.getInstance();
		year=cal.get(Calendar.YEAR);
		month=cal.get(Calendar.MONTH)+1;
		day=cal.get(Calendar.DATE);
		hour=cal.get(Calendar.HOUR_OF_DAY);
		minute=cal.get(Calendar.MINUTE);
		second=cal.get(Calendar.SECOND);
	}
	
	
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getActionCommand().equals(updatePassword_button.getText())){
				
				new UpdatePasswordFrame(empID,dbm).setVisible(true);
				
			}else if(e.getActionCommand().equals(signIn_button.getText())){
				
				int i = JOptionPane.showConfirmDialog(null, "确认签到", "提示",
						JOptionPane.YES_NO_OPTION);
				if(i==JOptionPane.YES_OPTION){
					String deptID=dbm.searchDeptIDByempID(empID);
					getCurrentTime();
					String Day=""+year+"-"+month+"-"+day;
					String SignInTime=""+hour+":"+minute+":"+second;
					boolean b=dbm.signIn(empID, deptID, Day, SignInTime);
				//	System.out.println(Day+"     "+SignInTime);
					if(b){
						JOptionPane.showMessageDialog(null, "签到成功", "Infomation",
								JOptionPane.INFORMATION_MESSAGE);
						signIn_button.setEnabled(false);
						signOut_button.setEnabled(true);
					}else{
						JOptionPane.showMessageDialog(null, "签到失败", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}else if(i==JOptionPane.NO_OPTION){
					
				}					
			}else if(e.getActionCommand().equals(signOut_button.getText())){

				int i = JOptionPane.showConfirmDialog(null, "确认签离", "提示",
						JOptionPane.YES_NO_OPTION);
				if(i==JOptionPane.YES_OPTION){
					getCurrentTime();
					String Day=""+year+"-"+month+"-"+day;
					String SignOutTime=""+hour+":"+minute+":"+second;
					String deptID=dbm.searchDeptIDByempID(empID);
					boolean b=dbm.signOut(empID, deptID, Day, SignOutTime);
					System.out.println("zzzzzzzz"+b);
					if(b){
						JOptionPane.showMessageDialog(null, "签离成功", "Infomation",
								JOptionPane.INFORMATION_MESSAGE);
						signOut_button.setEnabled(false);
					}else{
						JOptionPane.showMessageDialog(null, "签离失败", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}else if(i==JOptionPane.NO_OPTION){
					
				}
			}
		}
		
	}

}
