package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import structs.ChangeWorkInfo;
import structs.ChoiceBox;
import structs.ExcelOutput;

public class ChangeWorkPanel extends JPanel{
	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	private String belongPermission;
	
	String preDeptID;
	String preDutyID;
	String newDeptID;
	String newDutyID;
	String preDeptName;
	String preDutyName;
	String newDeptName;
	String newDutyName;
	String deptName;
	
	private static final int SEARCH_EMPRECORD_PANEL_WIDTH = 700;
	private static final int SEARCH_EMPRECORD_PANEL_HEIGHT = 270;
	
	String[] TableHeads = { "员工号", "员工姓名","原部门名","新部门名", "原职位名","新职位名", "调岗时间"};
	
	Object[][] infos={};
	ChangeWorkInfo[] changeInfos=null;
	
	String deptID;
	
	
	ChoiceBox empIDBox;
	String[] empinfos,dates;
	
	JPanel headPanel;
	private JButton search_emp_button,search_all_button;
	private JButton print_button;
	private JButton change_button;
	ChangeInfoTable mt;
	JTable t;
	
	public ChangeWorkPanel(String empID,int permission,DatabaseManage dbm,String belongPermission){
		this.setBounds(30, 10, SEARCH_EMPRECORD_PANEL_WIDTH, SEARCH_EMPRECORD_PANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		this.belongPermission=belongPermission;
		
		deptID=dbm.searchDeptIDByempID(empID);
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empinfos=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
			deptName=dbm.searchDeptNameBydeptID(deptID);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empinfos=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		
		empIDBox=new ChoiceBox(empinfos);
		empIDBox.addItem(empID);
		
		
		headPanel=new JPanel();
		search_emp_button=new JButton("按员工号查询");
		search_all_button=new JButton("查询全部");
		print_button=new JButton("打印信息");
		change_button=new JButton("输入调岗");
		
		search_emp_button.addActionListener(new MyButtonListener());
		search_all_button.addActionListener(new MyButtonListener());
		change_button.addActionListener(new MyButtonListener());
		print_button.addActionListener(new MyButtonListener());
		
		
		headPanel.add(empIDBox);
		headPanel.add(search_emp_button);
		headPanel.add(search_all_button);
		headPanel.add(change_button);
		headPanel.add(print_button);
		
		

		mt = new ChangeInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);

		s.setBackground(Color.white);
		this.setBackground(Color.white);
		this.add(s, BorderLayout.CENTER);	
		this.add(headPanel,BorderLayout.NORTH);
		
	}
	
	public void update(){
		if(permission==Constant.UserPermission.DIVISION_MANAGER_RIGHT){
			empinfos=dbm.searchAllEmpIDBydeptID(deptID,belongPermission);
			deptName=dbm.searchDeptNameBydeptID(deptID);
		}else if(permission==Constant.UserPermission.MANAGER_RIGHT){
			empinfos=dbm.searchAllDiEmpIDByPermission(belongPermission);
		}
		empIDBox.setInfos(empinfos);
		
		empIDBox.updateUI();
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==search_emp_button){
				String empID=(String)empIDBox.getSelectedItem();
				changeInfos=dbm.searchEmpChangeWorkByEmpID(empID);
				int length=changeInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){	
					preDeptID=changeInfos[i].getPreDeptID();
					newDeptID=changeInfos[i].getNewDeptID();
					preDutyID=changeInfos[i].getPreDutyID();
					newDutyID=changeInfos[i].getNewDutyID();
					infos[i][0]=changeInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);				
					infos[i][2]=dbm.searchDeptNameBydeptID(preDeptID);
					infos[i][3]=dbm.searchDeptNameBydeptID(newDeptID);
					infos[i][4]=dbm.searchDutyNameBydutyID(preDutyID);
					infos[i][5]=dbm.searchDutyNameBydutyID(newDutyID);
					infos[i][6]=changeInfos[i].getChangeTime();
				}
				mt.setInfos(infos);
				t.updateUI();			
			}else if(e.getSource()==search_all_button){
				changeInfos=dbm.searchAllEmpChangeWork();
				int length=changeInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					preDeptID=changeInfos[i].getPreDeptID();
					newDeptID=changeInfos[i].getNewDeptID();
					preDutyID=changeInfos[i].getPreDutyID();
					newDutyID=changeInfos[i].getNewDutyID();
					infos[i][0]=changeInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);				
					infos[i][2]=dbm.searchDeptNameBydeptID(preDeptID);
					infos[i][3]=dbm.searchDeptNameBydeptID(newDeptID);
					infos[i][4]=dbm.searchDutyNameBydutyID(preDutyID);
					infos[i][5]=dbm.searchDutyNameBydutyID(newDutyID);
					infos[i][6]=changeInfos[i].getChangeTime();
				}
				mt.setInfos(infos);
				t.updateUI();	
			}else if(e.getSource()==change_button){
				new UpdateWorkFrame((String)empIDBox.getSelectedItem(),dbm).setVisible(true);
			}else if(e.getSource()==print_button){
				ExcelOutput out=new ExcelOutput("员工调岗信息表",TableHeads);
				out.printExcel(infos);
			}
		}	
	}
	
	class ChangeInfoTable extends AbstractTableModel {

		Object[][] infos;
		
		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public ChangeInfoTable(Object[][] infos){
			this.infos=infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}
	}
}
