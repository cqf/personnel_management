package enterpriseManage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import structs.ChangeWorkInfo;
import structs.ChoiceBox;
import structs.DateChooser;

public class UpdateWorkFrame extends JFrame{
	private static final int WIDTH=400;
	private static final int HEIGHT = 500;
	
	private JLabel title_label;
	
	private JLabel emp_label;
	private JLabel preDept_label;
	private JLabel newDept_label;
	private JLabel preDuty_label;
	private JLabel newDuty_label;
	private JLabel time_label;
	
	private JLabel emp;
	private JLabel preDept;
	private ChoiceBox newDept_box;
	private JLabel preDuty;
	private ChoiceBox newDuty_box;
	private DateChooser date_chooser;
	
	private JButton confirm_button;
	
	String empID;
	String deptID;
	String dutyID;
	DatabaseManage dbm;
	
	public UpdateWorkFrame(String empID,DatabaseManage dbm){
		this.setBounds(300, 50, WIDTH, HEIGHT);
		this.setResizable(false);
		Container content = getContentPane();
		content.setLayout(null);
		
		this.empID=empID;
		this.dbm=dbm;
		deptID=dbm.searchDeptIDByempID(empID);
		System.out.println(deptID);
		
		title_label=new JLabel("输入调岗");
		title_label.setFont(new Font("楷体", Font.BOLD, 22));
		title_label.setBounds(150, 20, 300, 50);
		
		
		emp_label=new JLabel("员工号：");
		emp_label.setBounds(50,80,50,30);
		
		preDept_label=new JLabel("原部门：");
		preDept_label.setBounds(50,130,50,30);
		
		preDuty_label=new JLabel("原职位：");
		preDuty_label.setBounds(50,180,120,30);
		
		newDept_label=new JLabel("新部门：");
		newDept_label.setBounds(50,230,120,30);
		
		newDuty_label=new JLabel("新职位：");
		newDuty_label.setBounds(50,280,120,30);
		
		time_label=new JLabel("调岗时间：");
		time_label.setBounds(50,330,120,30);
		
		emp=new JLabel(empID);
		emp.setBounds(200, 80, 100, 30);
		
		preDept=new JLabel(dbm.searchDeptNameBydeptID(deptID));
		preDept.setBounds(200, 130, 100, 30);
		
		dutyID=dbm.searchDutyIdByDutyName(dbm.searchDutyNameByEmpIDDeptID(empID, deptID));
		preDuty=new JLabel(dbm.searchDutyNameByEmpIDDeptID(empID, deptID));
		preDuty.setBounds(200, 180, 100, 30);
		
		newDept_box=new ChoiceBox(dbm.searchAllDeptName());
		newDept_box.removeItem("MD");
		newDept_box.setBounds(200, 230, 100, 30);
		
		newDuty_box=new ChoiceBox(dbm.searchAllDutyName());
		newDuty_box.removeItem("manager");
		newDuty_box.setBounds(200, 280, 100, 30);
		
		date_chooser=new DateChooser();
		date_chooser.setBounds(200, 330, 100, 30);
		
		confirm_button=new JButton("确定");
		confirm_button.setBounds(150,400,100,30);
		confirm_button.addActionListener(new MyButtonListener());
		
		this.setBackground(Color.white);
		content.add(title_label);
		content.add(emp_label);
		content.add(preDept_label);
		content.add(preDuty_label);
		content.add(newDept_label);
		content.add(newDuty_box);
		content.add(newDept_box);
		content.add(newDuty_label);
		content.add(preDept);	
		content.add(preDuty);
		content.add(confirm_button);
		content.add(date_chooser);
		content.add(time_label);
		content.add(emp);
		
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource()==confirm_button){
				String newDept=(String)newDept_box.getSelectedItem();
				String newDuty=(String)newDuty_box.getSelectedItem();
				String time=date_chooser.getChooseTime();
				String havP=dbm.searchEmpPasswordByEmpID(empID);
				
				String newDeptID=dbm.searchDeptIdByDeptName(newDept);
				String newDutyID=dbm.searchDutyIdByDutyName(newDuty);
				
				ChangeWorkInfo change=new ChangeWorkInfo();
				change.setEmpID(empID);
				change.setPreDeptID(deptID);
				change.setPreDutyID(dutyID);
				change.setNewDeptID(newDeptID);
				change.setNewDutyID(newDutyID);
				change.setChangeTime(time);
				
				//System.out.println(newDept+"   "+newDeptID);
				
				boolean cb=dbm.addChangeWorkInfoToDB(change);
				boolean eb=dbm.updateEmployeeinfo(empID, newDeptID);
				boolean wb=dbm.updateWork(empID, newDeptID, newDutyID);
				
				if(cb&&eb&&wb){
					JOptionPane.showMessageDialog(null, "添加成功", "Information",
							JOptionPane.INFORMATION_MESSAGE);
				}else{
					JOptionPane.showMessageDialog(null, "添加失败", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
	}
	
}
