package enterpriseManage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import structs.ChoiceBox;
import structs.ExcelOutput;
import structs.RecordInfo;
import enterpriseManage.WorkRecordManagePanel.MyButtonListener;
import enterpriseManage.WorkRecordManagePanel.RecordInfoTable;

public class StaffWorkRecordPanel extends JPanel{
	private String empID;
	private int permission=Constant.UserPermission.NULL_RIGHT;
	private DatabaseManage dbm;
	
	private static final int SEARCH_EMPRECORD_PANEL_WIDTH = 700;
	private static final int SEARCH_EMPRECORD_PANEL_HEIGHT = 270;
	
	String[] TableHeads = { "员工号", "员工姓名","部门名", "日期","签到时间", "签离时间", "备注"};
	
	Object[][] infos={};
	RecordInfo[] recordInfos=null;
	
	String deptID;
	String deptName;
	
	ChoiceBox dateBOX;
	String[] dates;
	
	JPanel headPanel;
	private JButton search_date_button,search_all_button;
	private JButton print_button;
	RecordInfoTable mt;
	JTable t;
	
	public StaffWorkRecordPanel(String empID,int permission,DatabaseManage dbm){
		this.setBounds(30, 10, SEARCH_EMPRECORD_PANEL_WIDTH, SEARCH_EMPRECORD_PANEL_HEIGHT);
		this.setLayout(new BorderLayout());
		
		this.empID=empID;
		this.permission=permission;
		this.dbm=dbm;
		
		deptID=dbm.searchDeptIDByempID(empID);
		deptName=dbm.searchDeptNameBydeptID(deptID);
		
		dates=dbm.searchWorkRecordDate();
		dateBOX=new ChoiceBox(dates);
		
		headPanel=new JPanel();
		search_date_button=new JButton("按日期查询");
		search_all_button=new JButton("查询全部");
		print_button=new JButton("打印信息");
		
		search_date_button.addActionListener(new MyButtonListener());
		search_all_button.addActionListener(new MyButtonListener());
		print_button.addActionListener(new MyButtonListener());
		
	
		headPanel.add(dateBOX);
		headPanel.add(search_date_button);
		headPanel.add(search_all_button);
		headPanel.add(print_button);
		
		mt = new RecordInfoTable(infos);
		t = new JTable(mt);
		JScrollPane s = new JScrollPane(t);
		s.setBackground(Color.white);
		this.setBackground(Color.white);
		this.add(s, BorderLayout.CENTER);	
		this.add(headPanel,BorderLayout.NORTH);
		
	}
	
	public void update(){
		
		dates=dbm.searchWorkRecordDate();
		dateBOX.setInfos(dates);
		
		dateBOX.updateUI();
	}
	
	public class MyButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			  if(e.getSource()==search_date_button){
				String date=(String)dateBOX.getSelectedItem();
				recordInfos=dbm.searchSelfWorkRecordByDate(empID,date);
				int length=recordInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=recordInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=deptName;
					infos[i][3]=recordInfos[i].getDay();
					infos[i][4]=recordInfos[i].getSignIntime();
					infos[i][5]=recordInfos[i].getSignOutTime();
					infos[i][6]=recordInfos[i].getState();
				}
				mt.setInfos(infos);
				t.updateUI();		
			}else if(e.getSource()==search_all_button){
				recordInfos=dbm.searchEmpWorkRecordByEmpID(empID);
				int length=recordInfos.length;
				infos=new Object[length][TableHeads.length];
				
				for(int i=0;i<length;i++){		
					infos[i][0]=recordInfos[i].getEmpID();
					infos[i][1]=dbm.searchEmpNameByEmpID((String)infos[i][0]);
					infos[i][2]=deptName;
					infos[i][3]=recordInfos[i].getDay();
					infos[i][4]=recordInfos[i].getSignIntime();
					infos[i][5]=recordInfos[i].getSignOutTime();
					infos[i][6]=recordInfos[i].getState();
				}
				mt.setInfos(infos);
				t.updateUI();	
			}else if(e.getSource()==print_button){
				ExcelOutput out=new ExcelOutput("员工考勤表",TableHeads);
				out.printExcel(infos);
			}
		}	
	}
	
	class RecordInfoTable extends AbstractTableModel {

		Object[][] infos;
		
		public Object[][] getInfos() {
			return infos;
		}

		public void setInfos(Object[][] infos) {
			this.infos = infos;
		}

		public RecordInfoTable(Object[][] infos){
			this.infos=infos;
		}

		public int getColumnCount() {
			return TableHeads.length;
		}

		public int getRowCount() {
			return infos.length;
		}

		public String getColumnName(int col) {
			return TableHeads[col];
		}

		public Object getValueAt(int row, int col) {
			return infos[row][col];
		}
	}
	
}
