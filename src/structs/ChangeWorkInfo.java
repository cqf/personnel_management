package structs;

public class ChangeWorkInfo {

	private String empID;
	private String preDeptID;
	private String preDutyID;
	private String newDeptID;
	private String newDutyID;
	private String changeTime;
	
	public String getEmpID() {
		return empID;
	}
	public void setEmpID(String empID) {
		this.empID = empID;
	}
	public String getPreDeptID() {
		return preDeptID;
	}
	public void setPreDeptID(String preDeptID) {
		this.preDeptID = preDeptID;
	}
	public String getPreDutyID() {
		return preDutyID;
	}
	public void setPreDutyID(String preDutyID) {
		this.preDutyID = preDutyID;
	}
	public String getNewDeptID() {
		return newDeptID;
	}
	public void setNewDeptID(String newDeptID) {
		this.newDeptID = newDeptID;
	}
	public String getNewDutyID() {
		return newDutyID;
	}
	public void setNewDutyID(String newDutyID) {
		this.newDutyID = newDutyID;
	}
	public String getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(String changeTime) {
		this.changeTime = changeTime;
	}
	
	
	
}
