package structs;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;

public class ChoiceBox extends JComboBox implements ItemListener {

	private String[] infos;

	public ChoiceBox(String[] infos) {
		super();
		this.infos = infos;
		for (int i = 0; i < infos.length; i++) {
			this.addItem(infos[i]);
		}
	}

	public String[] getInfos() {
		return infos;
	}

	public void setInfos(String[] infos) {
		this.infos = infos;
	}

	public void itemStateChanged(ItemEvent e) {

	}
}