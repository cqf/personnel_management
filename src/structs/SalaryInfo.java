package structs;

public class SalaryInfo {

	private String empID;
	private String empName;
	private String deptID;
	private String Year;
	private String Month;
	private String basicSalary;
	private String Bonus;
	private String Fine;
	private String sumSalary;
	public String getEmpID() {
		return empID;
	}
	public void setEmpID(String empID) {
		this.empID = empID;
	}
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getDeptID() {
		return deptID;
	}
	public void setDeptID(String deptID) {
		this.deptID = deptID;
	}
	public String getYear() {
		return Year;
	}
	public void setYear(String year) {
		Year = year;
	}
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public String getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(String basicSalary) {
		this.basicSalary = basicSalary;
	}
	public String getBonus() {
		return Bonus;
	}
	public void setBonus(String bonus) {
		Bonus = bonus;
	}
	public String getFine() {
		return Fine;
	}
	public void setFine(String fine) {
		Fine = fine;
	}
	public String getSumSalary() {
		return sumSalary;
	}
	public void setSumSalary(String sumSalary) {
		this.sumSalary = sumSalary;
	}
	

}
