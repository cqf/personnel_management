package structs;

import java.io.FileOutputStream;

import javax.swing.JFileChooser;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelOutput {
	
	String filePath = null;
	String title=null;
	String[] cellTitle=null;

	public ExcelOutput(String title,String[] celltitle){
		this.title=title;
		this.cellTitle=celltitle;
	}
	
	public void printExcel(Object[][] objects){
		
		JFileChooser fileChooser = new JFileChooser("D:\\");
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fileChooser.showOpenDialog(fileChooser);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			filePath = fileChooser.getSelectedFile().getAbsolutePath();// 这个就是你选择的文件夹的路径
		}
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(title);
		HSSFRow row = sheet.createRow((int) 0);
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCell cell = null;
		
		for(int i=0;i<cellTitle.length;i++){
			cell = row.createCell((short) i);
			cell.setCellValue(cellTitle[i]);
			cell.setCellStyle(style);
		}

		for (int i = 0; i < objects.length; i++) {
			row = sheet.createRow((int) i + 1);
			for(int j=0;j<cellTitle.length;j++){
				row.createCell((short) j).setCellValue(
						(String) objects[i][j]);
			}
			
		}
		// 第六步，将文件存到指定位置
		try {
			FileOutputStream fout = new FileOutputStream(filePath
					+ "\\"+title+".xls");
			wb.write(fout);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
